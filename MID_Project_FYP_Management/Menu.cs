﻿using Guna.UI2.WinForms.Suite;
using MID_Project_FYP_Management.Helper_Classes;
using MID_Project_FYP_Management.UC_Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static Org.BouncyCastle.Math.EC.ECCurve;
using System.Text.RegularExpressions;
using System.Windows.Forms.VisualStyles;
using System.Security.Cryptography;



namespace MID_Project_FYP_Management
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            hideSubmenus_All();
        }

        #region Toggle
        private void hideSubmenus_All()
        {
            panelManagePeople.Visible = false;
            panelEvaluations.Visible = false;
            panelManageGroups.Visible = false;
            panelReports.Visible = false;
        }
        private void hide_Submenu_one()
        {
            if (panelManagePeople.Visible == true)
            {
                panelManagePeople.Visible = false;
            }
            if (panelManageGroups.Visible == true)
            {
                panelManageGroups.Visible = false;
            }
            if (panelEvaluations.Visible == true)
            {
                panelEvaluations.Visible = false;
            }
            if (panelReports.Visible == true)
            {
                panelReports.Visible = false;
            }
        }

        private void showSubMenu(Panel submenu)
        {
            if (submenu.Visible == false)
            {
                hide_Submenu_one();
                submenu.Visible = true;
            }
            else
            {
                submenu.Visible = false;
            }
        }

        #endregion

        #region mangePeople
        private void managePeople_btn_Click(object sender, EventArgs e)
        {
            showSubMenu(panelManagePeople);


        }

        private void student_btn_Click(object sender, EventArgs e)
        {
            //--code
            //UC_Student_CRUD student_CRUD = new UC_Student_CRUD();
            //UserControl_Helper.addUserControl(student_CRUD,panelParentContainer);

            var student_CRUD = new UC_CRUD_Student();
            UserControl_Helper.addUserControl(student_CRUD, panelParentContainer);

            hide_Submenu_one();
        }

        private void advisor_btn_Click(object sender, EventArgs e)
        {
            var uc = new UC_CRUD_Advisor();
            UserControl_Helper.addUserControl(uc, panelParentContainer);


            hide_Submenu_one();
        }
        #endregion

        #region Manage Project

        private void manageProjects_btn_Click(object sender, EventArgs e)
        {
            //--code
            //var uc = new UC_CRUD_Projects();
            var uc = new UC_CRUD_Projects();
            UserControl_Helper.addUserControl(uc, panelParentContainer);
        }

        #endregion

        #region ManageGroups
        private void manageGroups_btn_Click(object sender, EventArgs e)
        {
            showSubMenu(panelManageGroups);



        }

        private void group_btn_Click(object sender, EventArgs e)
        {
            hide_Submenu_one();
            var uc = new UC_CRUD_groups_TEST();
            UserControl_Helper.addUserControl(uc, panelParentContainer);
        }

        private void studentStatus_btn_Click(object sender, EventArgs e)
        {
            hide_Submenu_one();
            var uc = new UC_AssignProject();
            UserControl_Helper.addUserControl(uc, panelParentContainer);
        }

        #endregion



        #region Manage evaluation

        private void manageEvaluations_btn_Click(object sender, EventArgs e)
        {
            showSubMenu(panelEvaluations);
        }

        private void evaluation_Btn_Click(object sender, EventArgs e)
        {
            hide_Submenu_one();
            var uc = new UC_CRUD_Evaluations();
            UserControl_Helper.addUserControl(uc, panelParentContainer);
        }

        private void evalucateGroup_btn_Click(object sender, EventArgs e)
        {
            hide_Submenu_one();
            var uc = new UC_EvaluateGroup();
            UserControl_Helper.addUserControl(uc, panelParentContainer);

        }

        #endregion

        #region Reports

        private void reports_btn_Click(object sender, EventArgs e)
        {
            showSubMenu(panelReports);
            try
            {
                ExportDataTableToPdf(@"D:\test.pdf");
                //testFunction(@"D:\dummy.pdf");


                System.Diagnostics.Process.Start(@"D:\test.pdf");
                this.WindowState = System.Windows.Forms.FormWindowState.Minimized;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }


        #region PDF Report generation

        

        #region Events
        void ExportDataTableToPdf(String strPdfPath)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();
            

            //Fonts
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            //Row and Column Fonts
            iTextSharp.text.Font fntColumnHeader = new iTextSharp.text.Font(btnColumnHeader, 10, 1, BaseColor.BLACK);
            iTextSharp.text.Font rowFont = new iTextSharp.text.Font(bf, 11, iTextSharp.text.Font.NORMAL);
            //Heading & Para Font sizes
            iTextSharp.text.Font FirstHeading = new iTextSharp.text.Font(bf, 16, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font SecondHeading = new iTextSharp.text.Font(bf, 14, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font ThirdHeading = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font Parafont = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);
            //////////////////////////////
            ///
            string documentHeading = "Report";
            string headerText = "Summary";
            string footerText = "Computer Generated";
            //Header 
            PDF_Helper_Class.OnStartPage(writer, headerText);

            //DOCUMENT HEADING
            PDF_Helper_Class.Make_Main_Heading(ref document, documentHeading, FirstHeading);
            //DATE

            //LINE
            PDF_Helper_Class.AddLine_Break(ref document);
            #region PERSON (STUDENT , ADVISOR)
            //PERSON DETAILS
            PDF_Helper_Class.Make_Heading(ref document, "Person Details", 0, SecondHeading);
            PDF_Helper_Class.AddOneLine_Space(ref document);
            string personQuery = "SELECT P.Id, CONCAT(P.FirstName,' ',P.LastName) 'Name', P.Contact,P.Email,P.DateOfBirth FROM Person P";
            PDF_Helper_Class.addPDF_Table(ref document, personQuery, fntColumnHeader,rowFont);
            PDF_Helper_Class.AddOneLine_Space(ref document);

            PDF_Helper_Class.OnEndPage(writer, footerText);
            document.NewPage();
            PDF_Helper_Class.OnStartPage(writer, headerText);
            //STUDENT DETAILS
            PDF_Helper_Class.Make_Heading(ref document, "1.Student Details", 40, SecondHeading);
            PDF_Helper_Class.AddOneLine_Space(ref document);
            string studentQuery = $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Name', S.RegistrationNo 'RegNo',P.Contact,P.Email,P.DateOfBirth  'DOB' " +
                    $"FROM Person P " +
                    $"JOIN Student S ON S.Id=P.Id";
            PDF_Helper_Class.addPDF_Table(ref document, studentQuery, fntColumnHeader, rowFont);
            PDF_Helper_Class.AddOneLine_Space(ref document);

            PDF_Helper_Class.OnEndPage(writer, footerText);
            document.NewPage();
            PDF_Helper_Class.OnStartPage(writer, headerText);
            //ADVISOR DETAILS
            PDF_Helper_Class.Make_Heading(ref document, "2.Advisor Details", 40, SecondHeading);
            PDF_Helper_Class.AddOneLine_Space(ref document);
            string advisorQuery = $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Name', A.Designation ,A.Salary,P.Contact,P.Email,P.DateOfBirth  'DOB' " +
                    $"FROM Person P " +
                    $"JOIN Advisor A ON A.Id=P.Id";
            PDF_Helper_Class.addPDF_Table(ref document, advisorQuery, fntColumnHeader, rowFont);
            PDF_Helper_Class.AddOneLine_Space(ref document);

            PDF_Helper_Class.OnEndPage(writer, footerText);
            #endregion

            document.NewPage();
            PDF_Helper_Class.OnStartPage(writer, headerText);

            #region PROJECT DETAILS
            PDF_Helper_Class.Make_Heading(ref document, "Project Details", 0, SecondHeading);
            PDF_Helper_Class.AddOneLine_Space(ref document);
            string projectQuery = $"SELECT P.Title, P.Description" +
                    $"," +
                    $"(SELECT CONCAT(PER.FirstName,' ',PER.LastName) FROM ProjectAdvisor PA JOIN Person PER ON PER.Id=PA.AdvisorId JOIN LOOKUP L ON L.Id=PA.AdvisorRole WHERE L.VALUE='Main Advisor' AND ProjectId=P.Id) 'Main Advisor'" +
                    $",(SELECT CONCAT(PER.FirstName,' ',PER.LastName) FROM ProjectAdvisor PA JOIN Person PER ON PER.Id=PA.AdvisorId JOIN LOOKUP L ON L.Id=PA.AdvisorRole WHERE L.VALUE='Co-Advisror' AND ProjectId=P.Id) 'Co-Advisror'" +
                    $", ( SELECT CONCAT(PER.FirstName,' ',PER.LastName)  FROM ProjectAdvisor PA  JOIN Person PER ON PER.Id=PA.AdvisorId  JOIN LOOKUP L ON L.Id=PA.AdvisorRole WHERE L.VALUE='Industry Advisor' AND ProjectId=P.Id ) 'Industry Advisor'  " +
                    $"FROM Project P";
            PDF_Helper_Class.addPDF_Table(ref document, projectQuery, fntColumnHeader, rowFont);
            PDF_Helper_Class.AddOneLine_Space(ref document);

            PDF_Helper_Class.OnEndPage(writer, footerText);

            #endregion
            document.NewPage();
            PDF_Helper_Class.OnStartPage(writer, headerText);



            #region GROUP DETAILS
            PDF_Helper_Class.Make_Heading(ref document, "Group Details", 0, SecondHeading);
            PDF_Helper_Class.AddOneLine_Space(ref document);

            //Group Heading
            int numRows = 0;
            string groupQuery = "SELECT * FROM [ProjectA].[dbo].[Group]";
            DataTable dt= PDF_Helper_Class.MakeData_Table(groupQuery);
            foreach (DataRow row in dt.Rows)
            {
                string groupId = (row["Id"].ToString());
                //Group Heading
                PDF_Helper_Class.Make_Heading(ref document, (++numRows) + ". Group : ",40, SecondHeading);

                PDF_Helper_Class.AddOneLine_Space(ref document);
                //Group Members
                string g_MemberQuery = $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Name', S.RegistrationNo 'RegNo', L.Value 'Status', GS.AssignmentDate" +
                    $" FROM [GroupStudent] GS" +
                    $" JOIN Lookup L ON L.Id=GS.Status" +
                    $" JOIN Student S ON S.Id =GS.StudentId" +
                    $" JOIN Person P ON P.Id=S.Id\r\nWHERE GroupId ={groupId}";
                PDF_Helper_Class.addPDF_Table(ref document, g_MemberQuery, fntColumnHeader, rowFont);

            }

            #endregion

            document.NewPage();
            PDF_Helper_Class.OnStartPage(writer, headerText);

            #region EVALUATION DETAILS
            PDF_Helper_Class.Make_Heading(ref document, "Evaluation Details", 0, SecondHeading);
            PDF_Helper_Class.AddOneLine_Space(ref document);
            string evaluationQuery = $"SELECT E.Name, E.TotalMarks, E.TotalWeightage FROM Evaluation E";
            PDF_Helper_Class.addPDF_Table(ref document, evaluationQuery, fntColumnHeader, rowFont);
            PDF_Helper_Class.AddOneLine_Space(ref document);

            PDF_Helper_Class.OnEndPage(writer, footerText);
            #endregion

            #region GROUP EVALUATIONS
            document.NewPage();
            PDF_Helper_Class.OnStartPage(writer, headerText);

            PDF_Helper_Class.Make_Heading(ref document, "Group Evaluations ", 0, SecondHeading);
            PDF_Helper_Class.AddOneLine_Space(ref document);

            //Group Heading
            int n_Rows = 0;
            string groupProjectQuery = "SELECT * FROM [ProjectA].[dbo].[GroupProject]";
            DataTable dt_Gp = PDF_Helper_Class.MakeData_Table(groupProjectQuery);
            foreach (DataRow row in dt_Gp.Rows)
            {
                string groupId = (row["GroupId"].ToString());
                string projectId = (row["ProjectId"].ToString());
                //Group Heading
                PDF_Helper_Class.Make_Heading(ref document, (++n_Rows) + ". Group : ", 40, SecondHeading);
                //Group Project
                PDF_Helper_Class.Make_Heading(ref document, "Project : ", 60, SecondHeading);
                PDF_Helper_Class.AddOneLine_Space(ref document);
                string p_Query = $"SELECT P.Title, P.Description" +
                    $"," +
                    $"(SELECT CONCAT(PER.FirstName,' ',PER.LastName) FROM ProjectAdvisor PA JOIN Person PER ON PER.Id=PA.AdvisorId JOIN LOOKUP L ON L.Id=PA.AdvisorRole WHERE L.VALUE='Main Advisor' AND ProjectId=P.Id) 'Main Advisor'" +
                    $",(SELECT CONCAT(PER.FirstName,' ',PER.LastName) FROM ProjectAdvisor PA JOIN Person PER ON PER.Id=PA.AdvisorId JOIN LOOKUP L ON L.Id=PA.AdvisorRole WHERE L.VALUE='Co-Advisror' AND ProjectId=P.Id) 'Co-Advisror'" +
                    $", ( SELECT CONCAT(PER.FirstName,' ',PER.LastName)  FROM ProjectAdvisor PA  JOIN Person PER ON PER.Id=PA.AdvisorId  JOIN LOOKUP L ON L.Id=PA.AdvisorRole WHERE L.VALUE='Industry Advisor' AND ProjectId=P.Id ) 'Industry Advisor'  " +
                    $"FROM Project P " +
                    $"JOIN GroupProject GP ON GP.ProjectId=P.Id " +
                    $"WHERE GP.GroupId={groupId} ";
                PDF_Helper_Class.addPDF_Table(ref document, p_Query, fntColumnHeader, rowFont);
                    
                //Group Members
                PDF_Helper_Class.Make_Heading(ref document, "Group Members : ", 60, SecondHeading);
                PDF_Helper_Class.AddOneLine_Space(ref document);
                string g_MemberQuery = $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Name', S.RegistrationNo 'RegNo', L.Value 'Status', GS.AssignmentDate" +
                    $" FROM [GroupStudent] GS" +
                    $" JOIN Lookup L ON L.Id=GS.Status" +
                    $" JOIN Student S ON S.Id =GS.StudentId" +
                    $" JOIN Person P ON P.Id=S.Id\r\nWHERE GroupId ={groupId}";
                PDF_Helper_Class.addPDF_Table(ref document, g_MemberQuery, fntColumnHeader, rowFont);

                //Evaluation
                PDF_Helper_Class.Make_Heading(ref document, "Group Evaluations : ", 60, SecondHeading);
                PDF_Helper_Class.AddOneLine_Space(ref document);
                string g_EvaluationQuery = $"SELECT E.Name 'Evaluation Name', GE.ObtainedMarks ,E.TotalMarks ,E.TotalWeightage, GE.EvaluationDate " +
                $"FROM GroupEvaluation GE " +
                $"JOIN Evaluation E ON E.Id=GE.EvaluationId " +
                $"WHERE GroupId ={groupId}";
                PDF_Helper_Class.addPDF_Table(ref document, g_EvaluationQuery, fntColumnHeader, rowFont);

            }
            #endregion

            
            //FOOTER
            PDF_Helper_Class.OnEndPage(writer, footerText);

            document.Close();
            writer.Close();
            fs.Close();
            PDF_Helper_Class.AddPageNumber(@"D:\test.pdf");
        }
      
        #endregion

        #endregion
        #endregion





    }
}
