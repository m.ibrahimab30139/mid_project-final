﻿namespace MID_Project_FYP_Management
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.panelReports = new System.Windows.Forms.Panel();
            this.list_of_stu_btn = new System.Windows.Forms.Button();
            this.Marksheet_Projects_btn = new System.Windows.Forms.Button();
            this.reports_btn = new System.Windows.Forms.Button();
            this.panelEvaluations = new System.Windows.Forms.Panel();
            this.evalucateGroup_btn = new System.Windows.Forms.Button();
            this.evaluation_Btn = new System.Windows.Forms.Button();
            this.manageEvaluations_btn = new System.Windows.Forms.Button();
            this.panelManageGroups = new System.Windows.Forms.Panel();
            this.studentStatus_btn = new System.Windows.Forms.Button();
            this.group_btn = new System.Windows.Forms.Button();
            this.manageGroups_btn = new System.Windows.Forms.Button();
            this.manageProjects_btn = new System.Windows.Forms.Button();
            this.panelManagePeople = new System.Windows.Forms.Panel();
            this.advisor_btn = new System.Windows.Forms.Button();
            this.student_btn = new System.Windows.Forms.Button();
            this.managePeople_btn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelParentContainer = new System.Windows.Forms.Panel();
            this.panelSideMenu.SuspendLayout();
            this.panelReports.SuspendLayout();
            this.panelEvaluations.SuspendLayout();
            this.panelManageGroups.SuspendLayout();
            this.panelManagePeople.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(14)))), ((int)(((byte)(173)))));
            this.panelSideMenu.Controls.Add(this.panelReports);
            this.panelSideMenu.Controls.Add(this.reports_btn);
            this.panelSideMenu.Controls.Add(this.panelEvaluations);
            this.panelSideMenu.Controls.Add(this.manageEvaluations_btn);
            this.panelSideMenu.Controls.Add(this.panelManageGroups);
            this.panelSideMenu.Controls.Add(this.manageGroups_btn);
            this.panelSideMenu.Controls.Add(this.manageProjects_btn);
            this.panelSideMenu.Controls.Add(this.panelManagePeople);
            this.panelSideMenu.Controls.Add(this.managePeople_btn);
            this.panelSideMenu.Controls.Add(this.panel1);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(200, 563);
            this.panelSideMenu.TabIndex = 0;
            // 
            // panelReports
            // 
            this.panelReports.BackColor = System.Drawing.Color.Black;
            this.panelReports.Controls.Add(this.list_of_stu_btn);
            this.panelReports.Controls.Add(this.Marksheet_Projects_btn);
            this.panelReports.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelReports.Location = new System.Drawing.Point(0, 514);
            this.panelReports.Name = "panelReports";
            this.panelReports.Size = new System.Drawing.Size(183, 80);
            this.panelReports.TabIndex = 13;
            // 
            // list_of_stu_btn
            // 
            this.list_of_stu_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.list_of_stu_btn.FlatAppearance.BorderSize = 0;
            this.list_of_stu_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.list_of_stu_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.list_of_stu_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.list_of_stu_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.list_of_stu_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.list_of_stu_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.list_of_stu_btn.Location = new System.Drawing.Point(0, 35);
            this.list_of_stu_btn.Name = "list_of_stu_btn";
            this.list_of_stu_btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.list_of_stu_btn.Size = new System.Drawing.Size(183, 35);
            this.list_of_stu_btn.TabIndex = 5;
            this.list_of_stu_btn.Text = "List of Students";
            this.list_of_stu_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.list_of_stu_btn.UseVisualStyleBackColor = true;
            // 
            // Marksheet_Projects_btn
            // 
            this.Marksheet_Projects_btn.BackColor = System.Drawing.Color.Transparent;
            this.Marksheet_Projects_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.Marksheet_Projects_btn.FlatAppearance.BorderSize = 0;
            this.Marksheet_Projects_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.Marksheet_Projects_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.Marksheet_Projects_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Marksheet_Projects_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Marksheet_Projects_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Marksheet_Projects_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Marksheet_Projects_btn.Location = new System.Drawing.Point(0, 0);
            this.Marksheet_Projects_btn.Name = "Marksheet_Projects_btn";
            this.Marksheet_Projects_btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.Marksheet_Projects_btn.Size = new System.Drawing.Size(183, 35);
            this.Marksheet_Projects_btn.TabIndex = 4;
            this.Marksheet_Projects_btn.Text = "Marksheet Projects";
            this.Marksheet_Projects_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Marksheet_Projects_btn.UseVisualStyleBackColor = false;
            // 
            // reports_btn
            // 
            this.reports_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.reports_btn.FlatAppearance.BorderSize = 0;
            this.reports_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.reports_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.reports_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reports_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reports_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reports_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reports_btn.Location = new System.Drawing.Point(0, 479);
            this.reports_btn.Name = "reports_btn";
            this.reports_btn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.reports_btn.Size = new System.Drawing.Size(183, 35);
            this.reports_btn.TabIndex = 12;
            this.reports_btn.Text = "Generate Reports";
            this.reports_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reports_btn.UseVisualStyleBackColor = true;
            this.reports_btn.Click += new System.EventHandler(this.reports_btn_Click);
            // 
            // panelEvaluations
            // 
            this.panelEvaluations.BackColor = System.Drawing.Color.Black;
            this.panelEvaluations.Controls.Add(this.evalucateGroup_btn);
            this.panelEvaluations.Controls.Add(this.evaluation_Btn);
            this.panelEvaluations.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEvaluations.Location = new System.Drawing.Point(0, 399);
            this.panelEvaluations.Name = "panelEvaluations";
            this.panelEvaluations.Size = new System.Drawing.Size(183, 80);
            this.panelEvaluations.TabIndex = 11;
            // 
            // evalucateGroup_btn
            // 
            this.evalucateGroup_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.evalucateGroup_btn.FlatAppearance.BorderSize = 0;
            this.evalucateGroup_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.evalucateGroup_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.evalucateGroup_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.evalucateGroup_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evalucateGroup_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.evalucateGroup_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.evalucateGroup_btn.Location = new System.Drawing.Point(0, 35);
            this.evalucateGroup_btn.Name = "evalucateGroup_btn";
            this.evalucateGroup_btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.evalucateGroup_btn.Size = new System.Drawing.Size(183, 35);
            this.evalucateGroup_btn.TabIndex = 5;
            this.evalucateGroup_btn.Text = "Evaluate Group";
            this.evalucateGroup_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.evalucateGroup_btn.UseVisualStyleBackColor = true;
            this.evalucateGroup_btn.Click += new System.EventHandler(this.evalucateGroup_btn_Click);
            // 
            // evaluation_Btn
            // 
            this.evaluation_Btn.BackColor = System.Drawing.Color.Transparent;
            this.evaluation_Btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.evaluation_Btn.FlatAppearance.BorderSize = 0;
            this.evaluation_Btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.evaluation_Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.evaluation_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.evaluation_Btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluation_Btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.evaluation_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.evaluation_Btn.Location = new System.Drawing.Point(0, 0);
            this.evaluation_Btn.Name = "evaluation_Btn";
            this.evaluation_Btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.evaluation_Btn.Size = new System.Drawing.Size(183, 35);
            this.evaluation_Btn.TabIndex = 4;
            this.evaluation_Btn.Text = "Evaluation";
            this.evaluation_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.evaluation_Btn.UseVisualStyleBackColor = false;
            this.evaluation_Btn.Click += new System.EventHandler(this.evaluation_Btn_Click);
            // 
            // manageEvaluations_btn
            // 
            this.manageEvaluations_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.manageEvaluations_btn.FlatAppearance.BorderSize = 0;
            this.manageEvaluations_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.manageEvaluations_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.manageEvaluations_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.manageEvaluations_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manageEvaluations_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.manageEvaluations_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.manageEvaluations_btn.Location = new System.Drawing.Point(0, 364);
            this.manageEvaluations_btn.Name = "manageEvaluations_btn";
            this.manageEvaluations_btn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.manageEvaluations_btn.Size = new System.Drawing.Size(183, 35);
            this.manageEvaluations_btn.TabIndex = 10;
            this.manageEvaluations_btn.Text = "Manage Evaluations";
            this.manageEvaluations_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.manageEvaluations_btn.UseVisualStyleBackColor = true;
            this.manageEvaluations_btn.Click += new System.EventHandler(this.manageEvaluations_btn_Click);
            // 
            // panelManageGroups
            // 
            this.panelManageGroups.BackColor = System.Drawing.Color.Black;
            this.panelManageGroups.Controls.Add(this.studentStatus_btn);
            this.panelManageGroups.Controls.Add(this.group_btn);
            this.panelManageGroups.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelManageGroups.Location = new System.Drawing.Point(0, 284);
            this.panelManageGroups.Name = "panelManageGroups";
            this.panelManageGroups.Size = new System.Drawing.Size(183, 80);
            this.panelManageGroups.TabIndex = 6;
            // 
            // studentStatus_btn
            // 
            this.studentStatus_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.studentStatus_btn.FlatAppearance.BorderSize = 0;
            this.studentStatus_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.studentStatus_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.studentStatus_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.studentStatus_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentStatus_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.studentStatus_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.studentStatus_btn.Location = new System.Drawing.Point(0, 35);
            this.studentStatus_btn.Name = "studentStatus_btn";
            this.studentStatus_btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.studentStatus_btn.Size = new System.Drawing.Size(183, 35);
            this.studentStatus_btn.TabIndex = 5;
            this.studentStatus_btn.Text = "Assign Project";
            this.studentStatus_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.studentStatus_btn.UseVisualStyleBackColor = true;
            this.studentStatus_btn.Click += new System.EventHandler(this.studentStatus_btn_Click);
            // 
            // group_btn
            // 
            this.group_btn.BackColor = System.Drawing.Color.Transparent;
            this.group_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_btn.FlatAppearance.BorderSize = 0;
            this.group_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.group_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.group_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.group_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.group_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.group_btn.Location = new System.Drawing.Point(0, 0);
            this.group_btn.Name = "group_btn";
            this.group_btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.group_btn.Size = new System.Drawing.Size(183, 35);
            this.group_btn.TabIndex = 4;
            this.group_btn.Text = "Groups";
            this.group_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.group_btn.UseVisualStyleBackColor = false;
            this.group_btn.Click += new System.EventHandler(this.group_btn_Click);
            // 
            // manageGroups_btn
            // 
            this.manageGroups_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.manageGroups_btn.FlatAppearance.BorderSize = 0;
            this.manageGroups_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.manageGroups_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.manageGroups_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.manageGroups_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manageGroups_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.manageGroups_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.manageGroups_btn.Location = new System.Drawing.Point(0, 249);
            this.manageGroups_btn.Name = "manageGroups_btn";
            this.manageGroups_btn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.manageGroups_btn.Size = new System.Drawing.Size(183, 35);
            this.manageGroups_btn.TabIndex = 5;
            this.manageGroups_btn.Text = "Manage Groups\r\n";
            this.manageGroups_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.manageGroups_btn.UseVisualStyleBackColor = true;
            this.manageGroups_btn.Click += new System.EventHandler(this.manageGroups_btn_Click);
            // 
            // manageProjects_btn
            // 
            this.manageProjects_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.manageProjects_btn.FlatAppearance.BorderSize = 0;
            this.manageProjects_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.manageProjects_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.manageProjects_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.manageProjects_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manageProjects_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.manageProjects_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.manageProjects_btn.Location = new System.Drawing.Point(0, 214);
            this.manageProjects_btn.Name = "manageProjects_btn";
            this.manageProjects_btn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.manageProjects_btn.Size = new System.Drawing.Size(183, 35);
            this.manageProjects_btn.TabIndex = 4;
            this.manageProjects_btn.Text = "Manage Projects\r\n";
            this.manageProjects_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.manageProjects_btn.UseVisualStyleBackColor = true;
            this.manageProjects_btn.Click += new System.EventHandler(this.manageProjects_btn_Click);
            // 
            // panelManagePeople
            // 
            this.panelManagePeople.BackColor = System.Drawing.Color.Black;
            this.panelManagePeople.Controls.Add(this.advisor_btn);
            this.panelManagePeople.Controls.Add(this.student_btn);
            this.panelManagePeople.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelManagePeople.Location = new System.Drawing.Point(0, 133);
            this.panelManagePeople.Name = "panelManagePeople";
            this.panelManagePeople.Size = new System.Drawing.Size(183, 81);
            this.panelManagePeople.TabIndex = 3;
            // 
            // advisor_btn
            // 
            this.advisor_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.advisor_btn.FlatAppearance.BorderSize = 0;
            this.advisor_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.advisor_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.advisor_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.advisor_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advisor_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.advisor_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.advisor_btn.Location = new System.Drawing.Point(0, 35);
            this.advisor_btn.Name = "advisor_btn";
            this.advisor_btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.advisor_btn.Size = new System.Drawing.Size(183, 35);
            this.advisor_btn.TabIndex = 5;
            this.advisor_btn.Text = "Advisors\r\n";
            this.advisor_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.advisor_btn.UseVisualStyleBackColor = true;
            this.advisor_btn.Click += new System.EventHandler(this.advisor_btn_Click);
            // 
            // student_btn
            // 
            this.student_btn.BackColor = System.Drawing.Color.Transparent;
            this.student_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.student_btn.FlatAppearance.BorderSize = 0;
            this.student_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.student_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.student_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.student_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.student_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.student_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.student_btn.Location = new System.Drawing.Point(0, 0);
            this.student_btn.Name = "student_btn";
            this.student_btn.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.student_btn.Size = new System.Drawing.Size(183, 35);
            this.student_btn.TabIndex = 4;
            this.student_btn.Text = "Students\r\n";
            this.student_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.student_btn.UseVisualStyleBackColor = false;
            this.student_btn.Click += new System.EventHandler(this.student_btn_Click);
            // 
            // managePeople_btn
            // 
            this.managePeople_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.managePeople_btn.FlatAppearance.BorderSize = 0;
            this.managePeople_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.managePeople_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(17)))), ((int)(((byte)(211)))));
            this.managePeople_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.managePeople_btn.Font = new System.Drawing.Font("Nirmala UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.managePeople_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.managePeople_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.managePeople_btn.Location = new System.Drawing.Point(0, 98);
            this.managePeople_btn.Name = "managePeople_btn";
            this.managePeople_btn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.managePeople_btn.Size = new System.Drawing.Size(183, 35);
            this.managePeople_btn.TabIndex = 2;
            this.managePeople_btn.Text = "\tManage People";
            this.managePeople_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.managePeople_btn.UseVisualStyleBackColor = true;
            this.managePeople_btn.Click += new System.EventHandler(this.managePeople_btn_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(183, 98);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Nirmala UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(59, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "FYP MANAGEMENT";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MID_Project_FYP_Management.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(4, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 46);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panelParentContainer
            // 
            this.panelParentContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelParentContainer.Location = new System.Drawing.Point(200, 0);
            this.panelParentContainer.Name = "panelParentContainer";
            this.panelParentContainer.Size = new System.Drawing.Size(704, 563);
            this.panelParentContainer.TabIndex = 1;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 563);
            this.Controls.Add(this.panelParentContainer);
            this.Controls.Add(this.panelSideMenu);
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelSideMenu.ResumeLayout(false);
            this.panelReports.ResumeLayout(false);
            this.panelEvaluations.ResumeLayout(false);
            this.panelManageGroups.ResumeLayout(false);
            this.panelManagePeople.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelParentContainer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button managePeople_btn;
        private System.Windows.Forms.Button manageProjects_btn;
        private System.Windows.Forms.Panel panelManagePeople;
        private System.Windows.Forms.Button advisor_btn;
        private System.Windows.Forms.Button student_btn;
        private System.Windows.Forms.Button manageGroups_btn;
        private System.Windows.Forms.Panel panelManageGroups;
        private System.Windows.Forms.Button studentStatus_btn;
        private System.Windows.Forms.Button group_btn;
        private System.Windows.Forms.Panel panelReports;
        private System.Windows.Forms.Button list_of_stu_btn;
        private System.Windows.Forms.Button Marksheet_Projects_btn;
        private System.Windows.Forms.Button reports_btn;
        private System.Windows.Forms.Panel panelEvaluations;
        private System.Windows.Forms.Button evalucateGroup_btn;
        private System.Windows.Forms.Button evaluation_Btn;
        private System.Windows.Forms.Button manageEvaluations_btn;
    }
}