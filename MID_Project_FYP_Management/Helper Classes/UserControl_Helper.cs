﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MID_Project_FYP_Management.Helper_Classes
{
    public class UserControl_Helper
    {
        public static void addUserControl(Control uc, Control panelParentContainer)
        {
            panelParentContainer.Controls.Clear();

            uc.Dock = DockStyle.Fill;
            uc.BringToFront();
            uc.Focus();

            panelParentContainer.Controls.Add(uc);
        }
    }
}
