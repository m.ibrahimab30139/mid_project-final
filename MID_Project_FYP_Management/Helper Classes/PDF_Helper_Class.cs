﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Management.Instrumentation;
using System.IO;

namespace MID_Project_FYP_Management.Helper_Classes
{
    public class PDF_Helper_Class
    {
        #region Header & Footer
        public static void OnStartPage(PdfWriter writer, string headerText)
        {
            Paragraph footer = new Paragraph(headerText, FontFactory.GetFont(FontFactory.TIMES, 10, iTextSharp.text.Font.ITALIC, BaseColor.GRAY));
            footer.Alignment = Element.ALIGN_LEFT;
            PdfPTable footerTbl = new PdfPTable(1);
            footerTbl.TotalWidth = 300;
            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell cell = new PdfPCell(footer);
            cell.Border = 0;
            cell.PaddingRight = 5;

            footerTbl.AddCell(cell);
            footerTbl.WriteSelectedRows(0, -1, 35, 820, writer.DirectContent);
        }
        public static void OnEndPage(PdfWriter writer, string footerText)
        {
            //PdfPTableFooter pdfPTable = new PdfPTableFooter();
            ////int pageN = writer.PageNumber;
            ////String text = "Page " + pageN ;
            ////Paragraph footer = new Paragraph(text, FontFactory.GetFont(FontFactory.TIMES, 9, iTextSharp.text.Font.NORMAL, BaseColor.GRAY));
            //footer.Alignment = Element.ALIGN_RIGHT;
            //PdfPTable footerTbl = new PdfPTable(1);
            //footerTbl.TotalWidth = 300;
            //footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

            //PdfPCell cell = new PdfPCell(footer);
            //cell.Border = 0;
            //cell.PaddingLeft = 5;

            //footerTbl.AddCell(cell);
            //footerTbl.WriteSelectedRows(0, -1, 500, 30, writer.DirectContent);
        }

        #endregion

        #region Data Table
        public static DataTable MakeData_Table(string query) 
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-5IAQ2BP;Initial Catalog=ProjectA;Integrated Security=True");
            SqlCommand cmd = new SqlCommand(query);
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = conn;
            sda.SelectCommand = cmd;
            using (DataTable dt = new DataTable())
            {
                sda.Fill(dt);
                return dt;
            }
        }
        #endregion

        #region Headings
        public static void Make_Main_Heading(ref Document document, string headingText,iTextSharp.text.Font font) 
        {
            Paragraph prgHeading = new Paragraph();
            prgHeading.Alignment = Element.ALIGN_CENTER;
            prgHeading.Add(new Chunk(headingText, font));
            document.Add(prgHeading);
        }
        public static void Make_Heading(ref Document document, string headingText, float indent,iTextSharp.text.Font font)
        {
            Paragraph prgHeading = new Paragraph();
            prgHeading.Alignment = Element.ALIGN_LEFT;
            prgHeading.IndentationLeft = indent;
            prgHeading.Add(new Chunk("\n"+headingText, font));
            document.Add(prgHeading);
        }
        #endregion

        #region Formating
        public static void AddOneLine_Space(ref Document document) 
        {
            Paragraph p_Space = new Paragraph(new Chunk("\n"));
            document.Add(p_Space);
        }
        public static void AddLine_Break(ref Document document) 
        {
            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            document.Add(p);
        }
        public static void AddPageNumber(string src)
        {
            byte[] bytes = File.ReadAllBytes(src);
            iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.ITALIC, BaseColor.GRAY);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString(), blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(@"D:\test.pdf", bytes);
        }
        #endregion

        #region PDF Table
        public static void addPDF_Table(ref Document document, string query, iTextSharp.text.Font fnt_ColumnHeader, iTextSharp.text.Font fnt_Row) 
        {
            DataTable dt= MakeData_Table(query);
            PdfPTable table = new PdfPTable(dt.Columns.Count);
            int row = dt.Rows.Count;

            if (row != 0)
            {
               
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    PdfPCell cell = new PdfPCell();
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    cell.AddElement(new Chunk(dt.Columns[i].ColumnName, fnt_ColumnHeader));
                    table.AddCell(cell);
                }
                //table Data
                    for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        PdfPCell cell = new PdfPCell();
                        cell.AddElement(new Chunk(dt.Rows[i][j].ToString(), fnt_Row));
                        table.AddCell(cell);
                    }
                }

                    document.Add(table);
            }
            else
            {
                var prgHeading = new Paragraph();
                prgHeading.Alignment = Element.ALIGN_LEFT;
                prgHeading.IndentationLeft = 70;
                prgHeading.Add(new Chunk("No evaluation for this group has been conducted", fnt_Row));
                document.Add(prgHeading);
            }
        }
        #endregion
     
    }
}
