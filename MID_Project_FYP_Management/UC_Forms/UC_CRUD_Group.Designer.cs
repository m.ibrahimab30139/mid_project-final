﻿namespace MID_Project_FYP_Management.UC_Forms
{
    partial class UC_CRUD_Group
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView_Groups = new System.Windows.Forms.DataGridView();
            this.finalizeGroup_btn = new Guna.UI2.WinForms.Guna2Button();
            this.dataGridView_GroupDetails = new Guna.UI2.WinForms.Guna2DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Add_Member_btn = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutGroupPanel = new System.Windows.Forms.TableLayoutPanel();
            this.makeGroup_btn = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutTitlePanel = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView_Unselected_Students = new System.Windows.Forms.DataGridView();
            this.search_btn = new Guna.UI2.WinForms.Guna2Button();
            this.search_box_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.tableLayoutSearchPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutMainPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutSelectStudentPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutParentPanel = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Groups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_GroupDetails)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutGroupPanel.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutTitlePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Unselected_Students)).BeginInit();
            this.tableLayoutSearchPanel.SuspendLayout();
            this.tableLayoutMainPanel.SuspendLayout();
            this.tableLayoutSelectStudentPanel.SuspendLayout();
            this.tableLayoutParentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView_Groups
            // 
            this.dataGridView_Groups.AllowUserToAddRows = false;
            this.dataGridView_Groups.AllowUserToDeleteRows = false;
            this.dataGridView_Groups.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Groups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Groups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_Groups.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_Groups.Name = "dataGridView_Groups";
            this.dataGridView_Groups.ReadOnly = true;
            this.dataGridView_Groups.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_Groups.Size = new System.Drawing.Size(274, 229);
            this.dataGridView_Groups.TabIndex = 3;
            // 
            // finalizeGroup_btn
            // 
            this.finalizeGroup_btn.BorderColor = System.Drawing.Color.Blue;
            this.finalizeGroup_btn.BorderRadius = 5;
            this.finalizeGroup_btn.BorderThickness = 1;
            this.finalizeGroup_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.finalizeGroup_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.finalizeGroup_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.finalizeGroup_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.finalizeGroup_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.finalizeGroup_btn.FillColor = System.Drawing.Color.White;
            this.finalizeGroup_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finalizeGroup_btn.ForeColor = System.Drawing.Color.Black;
            this.finalizeGroup_btn.Location = new System.Drawing.Point(608, 3);
            this.finalizeGroup_btn.Name = "finalizeGroup_btn";
            this.finalizeGroup_btn.Size = new System.Drawing.Size(140, 30);
            this.finalizeGroup_btn.TabIndex = 11;
            this.finalizeGroup_btn.Text = "Finalize Group";
            // 
            // dataGridView_GroupDetails
            // 
            this.dataGridView_GroupDetails.AllowUserToAddRows = false;
            this.dataGridView_GroupDetails.AllowUserToDeleteRows = false;
            this.dataGridView_GroupDetails.AllowUserToResizeColumns = false;
            this.dataGridView_GroupDetails.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView_GroupDetails.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_GroupDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_GroupDetails.ColumnHeadersHeight = 15;
            this.dataGridView_GroupDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_GroupDetails.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_GroupDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_GroupDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_GroupDetails.Location = new System.Drawing.Point(283, 3);
            this.dataGridView_GroupDetails.Name = "dataGridView_GroupDetails";
            this.dataGridView_GroupDetails.ReadOnly = true;
            this.dataGridView_GroupDetails.RowHeadersVisible = false;
            this.dataGridView_GroupDetails.Size = new System.Drawing.Size(751, 229);
            this.dataGridView_GroupDetails.TabIndex = 1;
            this.dataGridView_GroupDetails.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_GroupDetails.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dataGridView_GroupDetails.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dataGridView_GroupDetails.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dataGridView_GroupDetails.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dataGridView_GroupDetails.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_GroupDetails.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_GroupDetails.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.dataGridView_GroupDetails.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView_GroupDetails.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_GroupDetails.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dataGridView_GroupDetails.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dataGridView_GroupDetails.ThemeStyle.HeaderStyle.Height = 15;
            this.dataGridView_GroupDetails.ThemeStyle.ReadOnly = true;
            this.dataGridView_GroupDetails.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_GroupDetails.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView_GroupDetails.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_GroupDetails.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_GroupDetails.ThemeStyle.RowsStyle.Height = 22;
            this.dataGridView_GroupDetails.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_GroupDetails.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.finalizeGroup_btn, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Add_Member_btn, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(283, 238);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(751, 36);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // Add_Member_btn
            // 
            this.Add_Member_btn.BorderColor = System.Drawing.Color.Blue;
            this.Add_Member_btn.BorderRadius = 5;
            this.Add_Member_btn.BorderThickness = 1;
            this.Add_Member_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Add_Member_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Add_Member_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Add_Member_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Add_Member_btn.Dock = System.Windows.Forms.DockStyle.Left;
            this.Add_Member_btn.FillColor = System.Drawing.Color.White;
            this.Add_Member_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_Member_btn.ForeColor = System.Drawing.Color.Black;
            this.Add_Member_btn.Location = new System.Drawing.Point(3, 3);
            this.Add_Member_btn.Name = "Add_Member_btn";
            this.Add_Member_btn.Size = new System.Drawing.Size(140, 30);
            this.Add_Member_btn.TabIndex = 10;
            this.Add_Member_btn.Text = "+ Add Memeber";
            // 
            // tableLayoutGroupPanel
            // 
            this.tableLayoutGroupPanel.ColumnCount = 2;
            this.tableLayoutGroupPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.00097F));
            this.tableLayoutGroupPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.99904F));
            this.tableLayoutGroupPanel.Controls.Add(this.dataGridView_GroupDetails, 1, 0);
            this.tableLayoutGroupPanel.Controls.Add(this.tableLayoutPanel1, 1, 1);
            this.tableLayoutGroupPanel.Controls.Add(this.dataGridView_Groups, 0, 0);
            this.tableLayoutGroupPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutGroupPanel.Location = new System.Drawing.Point(3, 384);
            this.tableLayoutGroupPanel.Name = "tableLayoutGroupPanel";
            this.tableLayoutGroupPanel.RowCount = 2;
            this.tableLayoutGroupPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutGroupPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutGroupPanel.Size = new System.Drawing.Size(1037, 277);
            this.tableLayoutGroupPanel.TabIndex = 3;
            // 
            // makeGroup_btn
            // 
            this.makeGroup_btn.BorderColor = System.Drawing.Color.Blue;
            this.makeGroup_btn.BorderRadius = 5;
            this.makeGroup_btn.BorderThickness = 1;
            this.makeGroup_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.makeGroup_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.makeGroup_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.makeGroup_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.makeGroup_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.makeGroup_btn.FillColor = System.Drawing.Color.White;
            this.makeGroup_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.makeGroup_btn.ForeColor = System.Drawing.Color.Black;
            this.makeGroup_btn.Location = new System.Drawing.Point(3, 3);
            this.makeGroup_btn.Name = "makeGroup_btn";
            this.makeGroup_btn.Size = new System.Drawing.Size(144, 32);
            this.makeGroup_btn.TabIndex = 10;
            this.makeGroup_btn.Text = "Make Group";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.125F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.875F));
            this.tableLayoutPanel7.Controls.Add(this.makeGroup_btn, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(842, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.39623F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.60378F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(192, 211);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Nirmala UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label7.Location = new System.Drawing.Point(348, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(339, 60);
            this.label7.TabIndex = 2;
            this.label7.Text = "Manage Groups";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutTitlePanel
            // 
            this.tableLayoutTitlePanel.ColumnCount = 3;
            this.tableLayoutTitlePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutTitlePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutTitlePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutTitlePanel.Controls.Add(this.label7, 1, 0);
            this.tableLayoutTitlePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutTitlePanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutTitlePanel.Name = "tableLayoutTitlePanel";
            this.tableLayoutTitlePanel.RowCount = 1;
            this.tableLayoutTitlePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTitlePanel.Size = new System.Drawing.Size(1037, 60);
            this.tableLayoutTitlePanel.TabIndex = 0;
            // 
            // dataGridView_Unselected_Students
            // 
            this.dataGridView_Unselected_Students.AllowUserToAddRows = false;
            this.dataGridView_Unselected_Students.AllowUserToDeleteRows = false;
            this.dataGridView_Unselected_Students.AllowUserToResizeColumns = false;
            this.dataGridView_Unselected_Students.AllowUserToResizeRows = false;
            this.dataGridView_Unselected_Students.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Unselected_Students.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Unselected_Students.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_Unselected_Students.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_Unselected_Students.Name = "dataGridView_Unselected_Students";
            this.dataGridView_Unselected_Students.ReadOnly = true;
            this.dataGridView_Unselected_Students.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_Unselected_Students.Size = new System.Drawing.Size(833, 211);
            this.dataGridView_Unselected_Students.TabIndex = 2;
            // 
            // search_btn
            // 
            this.search_btn.BorderColor = System.Drawing.Color.Blue;
            this.search_btn.BorderRadius = 5;
            this.search_btn.BorderThickness = 1;
            this.search_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.search_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.search_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.search_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.search_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.search_btn.FillColor = System.Drawing.Color.White;
            this.search_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search_btn.ForeColor = System.Drawing.Color.Black;
            this.search_btn.Location = new System.Drawing.Point(894, 3);
            this.search_btn.Name = "search_btn";
            this.search_btn.Size = new System.Drawing.Size(140, 31);
            this.search_btn.TabIndex = 9;
            this.search_btn.Text = "Search";
            // 
            // search_box_txt
            // 
            this.search_box_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.search_box_txt.DefaultText = "";
            this.search_box_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.search_box_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.search_box_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.search_box_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.search_box_txt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.search_box_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.search_box_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.search_box_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.search_box_txt.Location = new System.Drawing.Point(3, 3);
            this.search_box_txt.Name = "search_box_txt";
            this.search_box_txt.PasswordChar = '\0';
            this.search_box_txt.PlaceholderText = "Search Student by Name";
            this.search_box_txt.SelectedText = "";
            this.search_box_txt.Size = new System.Drawing.Size(683, 31);
            this.search_box_txt.TabIndex = 0;
            // 
            // tableLayoutSearchPanel
            // 
            this.tableLayoutSearchPanel.ColumnCount = 2;
            this.tableLayoutSearchPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.44166F));
            this.tableLayoutSearchPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.55834F));
            this.tableLayoutSearchPanel.Controls.Add(this.search_btn, 1, 0);
            this.tableLayoutSearchPanel.Controls.Add(this.search_box_txt, 0, 0);
            this.tableLayoutSearchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutSearchPanel.Location = new System.Drawing.Point(3, 69);
            this.tableLayoutSearchPanel.Name = "tableLayoutSearchPanel";
            this.tableLayoutSearchPanel.RowCount = 2;
            this.tableLayoutSearchPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.66667F));
            this.tableLayoutSearchPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.33333F));
            this.tableLayoutSearchPanel.Size = new System.Drawing.Size(1037, 60);
            this.tableLayoutSearchPanel.TabIndex = 1;
            // 
            // tableLayoutMainPanel
            // 
            this.tableLayoutMainPanel.ColumnCount = 1;
            this.tableLayoutMainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMainPanel.Controls.Add(this.tableLayoutSearchPanel, 0, 1);
            this.tableLayoutMainPanel.Controls.Add(this.tableLayoutTitlePanel, 0, 0);
            this.tableLayoutMainPanel.Controls.Add(this.tableLayoutSelectStudentPanel, 0, 2);
            this.tableLayoutMainPanel.Controls.Add(this.tableLayoutGroupPanel, 0, 3);
            this.tableLayoutMainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutMainPanel.Location = new System.Drawing.Point(61, 40);
            this.tableLayoutMainPanel.Name = "tableLayoutMainPanel";
            this.tableLayoutMainPanel.RowCount = 4;
            this.tableLayoutMainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutMainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutMainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutMainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.5F));
            this.tableLayoutMainPanel.Size = new System.Drawing.Size(1043, 664);
            this.tableLayoutMainPanel.TabIndex = 0;
            // 
            // tableLayoutSelectStudentPanel
            // 
            this.tableLayoutSelectStudentPanel.ColumnCount = 2;
            this.tableLayoutSelectStudentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.90646F));
            this.tableLayoutSelectStudentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.09354F));
            this.tableLayoutSelectStudentPanel.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutSelectStudentPanel.Controls.Add(this.dataGridView_Unselected_Students, 0, 0);
            this.tableLayoutSelectStudentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutSelectStudentPanel.Location = new System.Drawing.Point(3, 135);
            this.tableLayoutSelectStudentPanel.Name = "tableLayoutSelectStudentPanel";
            this.tableLayoutSelectStudentPanel.RowCount = 2;
            this.tableLayoutSelectStudentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.71194F));
            this.tableLayoutSelectStudentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.28807F));
            this.tableLayoutSelectStudentPanel.Size = new System.Drawing.Size(1037, 243);
            this.tableLayoutSelectStudentPanel.TabIndex = 2;
            // 
            // tableLayoutParentPanel
            // 
            this.tableLayoutParentPanel.ColumnCount = 3;
            this.tableLayoutParentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutParentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.Controls.Add(this.tableLayoutMainPanel, 1, 1);
            this.tableLayoutParentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutParentPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutParentPanel.Name = "tableLayoutParentPanel";
            this.tableLayoutParentPanel.RowCount = 3;
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.Size = new System.Drawing.Size(1166, 745);
            this.tableLayoutParentPanel.TabIndex = 1;
            // 
            // UC_CRUD_Group
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutParentPanel);
            this.Name = "UC_CRUD_Group";
            this.Size = new System.Drawing.Size(1166, 745);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Groups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_GroupDetails)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutGroupPanel.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutTitlePanel.ResumeLayout(false);
            this.tableLayoutTitlePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Unselected_Students)).EndInit();
            this.tableLayoutSearchPanel.ResumeLayout(false);
            this.tableLayoutMainPanel.ResumeLayout(false);
            this.tableLayoutSelectStudentPanel.ResumeLayout(false);
            this.tableLayoutParentPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Groups;
        private Guna.UI2.WinForms.Guna2Button finalizeGroup_btn;
        private Guna.UI2.WinForms.Guna2DataGridView dataGridView_GroupDetails;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Guna.UI2.WinForms.Guna2Button Add_Member_btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutGroupPanel;
        private Guna.UI2.WinForms.Guna2Button makeGroup_btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTitlePanel;
        private System.Windows.Forms.DataGridView dataGridView_Unselected_Students;
        private Guna.UI2.WinForms.Guna2Button search_btn;
        private Guna.UI2.WinForms.Guna2TextBox search_box_txt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSearchPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutMainPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSelectStudentPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutParentPanel;
    }
}
