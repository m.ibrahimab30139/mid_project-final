﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_CRUD_Student : UserControl
    {
        #region variables
        static readonly string cs = ConfigurationManager.ConnectionStrings["dbProjectA"].ConnectionString;
        readonly SqlConnection con = new SqlConnection(cs);
        readonly string emailPattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

        #endregion

        public UC_CRUD_Student()
        {
            InitializeComponent();
            fill_GenderCombox();
            gender_combox.SelectedIndex= 0;
            id_txt.Enabled = false;


            //DG data binding
            bindData_DG();

            //disable buttons
            if (string.IsNullOrEmpty(F_Name_txt.Text)) 
            {
                disable_Add_btn();
            }
           
        }
        private void F_Name_txt_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(F_Name_txt.Text)) 
            {
                enable_Add_Edit_btn();
            }
        }
        private void UC_CRUD_Student_Load(object sender, EventArgs e)
        {
           
        }

        #region Helper Functions

        private void disable_Add_btn() 
        {
            addStu_btn.Enabled = false;
          
        }
        private void enable_Add_Edit_btn()
        {
            addStu_btn.Enabled = true;
        }

        #endregion

        #region DataGrid View functions

        private void bindData_DG()
        {
            SqlCommand cmd = new SqlCommand(
                "Select s.id,s.RegistrationNo,p.FirstName, p.LastName,p.Email,L.value,p.DateOfBirth,p.Contact " + " from Student s " + " join Person p " + " on p.id=s.id join Lookup L on L.Id=P.Gender" , con); //join multiple table person and student

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView_Students.DataSource = dt;
            dataGridView_Students.Refresh();
        }
        private void dataGridView_Students_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            F_Name_txt.Text = dataGridView_Students.SelectedRows[0].Cells[2].Value.ToString();
            L_Name_txt.Text = dataGridView_Students.SelectedRows[0].Cells[3].Value.ToString();

            contact_txt.Text = dataGridView_Students.SelectedRows[0].Cells[7].Value.ToString();

            email_txt.Text = dataGridView_Students.SelectedRows[0].Cells[4].Value.ToString();
            regNo_txt.Text = dataGridView_Students.SelectedRows[0].Cells[1].Value.ToString();
            id_txt.Text = dataGridView_Students.SelectedRows[0].Cells[0].Value.ToString();

            gender_combox.Text = dataGridView_Students.SelectedRows[0].Cells[5].Value.ToString();
            dob_picker.Text = dataGridView_Students.SelectedRows[0].Cells[6].Value.ToString();
        }

        #endregion

        #region Data Filling Functions

        private void fill_GenderCombox()
        {
            string errorMessege = "\nWhile binding gender combobox";
            try
            {
                string query = "select * from Lookup where category='Gender'";
                SqlCommand sqlCommand = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    string gender = dataReader.GetString(1); //Because value is at 1 index
                    gender_combox.Items.Add(gender);
                }
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Format functions

        private void form_Clear()
        {
            contact_txt.Text = "";
            email_txt.Text = "";
            regNo_txt.Text = "";
            F_Name_txt.Text = "";
            L_Name_txt.Text = "";

        }

        #endregion

        #region Database attribute getting Functions
        private string getLookup_Details(string category, string value, string requiredAttribute)
        {
            string id = "";
            string errorMessege = $"\nWhile geting {category} from lookupTable";
            try
            {
                string query = $"SELECT {requiredAttribute} FROM Lookup WHERE Lookup.Value='{value}'";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                id = cmd.ExecuteScalar().ToString();
                con.Close();
                return id;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return id;
        }

        private bool Check_DuplicateNames(string name) 
        {
            string[] studentName = name.Split(' ');
            string f_Name = studentName[0];
            string l_name = studentName[1];
            string errorMessege = "\nWhile 'checking name duplicates'";
            int id = -1; //For error purposes

            string query = $" select id " +
                $" from Person " +
                $" where " +
                $" FirstName = '{f_Name}' and " +
                $" LastName = '{l_name}'";
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            //MessageBox.Show(id.ToString());
            if (id == -1) { return false; }
            else { return true; }

        }
        private int getIdFormPersonTable(int gender)
        {
            string errorMessege = "\nWhile 'getting Person Id' Student";
            int id = -1; //For error purposes
          
                string query = $" select id " +
                    $" from Person " +
                    $" where " +
                    $" FirstName = '{F_Name_txt.Text}' and " +
                    $" LastName = '{L_Name_txt.Text}' and " +
                    $" contact = '{contact_txt.Text}' and " +
                    $" email = '{email_txt.Text}' and " +
                    $" Gender = '{gender}'";
            
                SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return id;
        }

        #endregion

        #region Data Validation

        private bool Is_ValidData() 
        {
            bool is_Valid = true;

            if(string.IsNullOrEmpty(contact_txt.Text)) { is_Valid = false; }
            if(string.IsNullOrEmpty(F_Name_txt.Text)) { is_Valid = false; }
            if(string.IsNullOrEmpty(L_Name_txt.Text)) { is_Valid = false; }
            if(string.IsNullOrEmpty(regNo_txt.Text)) { is_Valid = false; }
            if(string.IsNullOrEmpty(email_txt.Text)) { is_Valid = false; }

            return is_Valid;
        }
        private void F_Name_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void F_Name_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(F_Name_txt.Text))
            {
                F_Name_txt.Focus();
                eProvider_F_name.SetError(F_Name_txt, "Enter a first name");
                disable_Add_btn();
            }
            else
            {
                eProvider_F_name.Clear();
                enable_Add_Edit_btn();
            }
        }

        private void L_Name_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(F_Name_txt.Text))
            {
                L_Name_txt.Focus();
                eProvider_L_name.SetError(L_Name_txt, "Enter a last name");
                disable_Add_btn();
            }
            else
            {
                eProvider_L_name.Clear();
                enable_Add_Edit_btn();
            }
        }
        private void L_Name_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void contact_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(contact_txt.Text))
            {
                contact_txt.Focus();
                eProvider_contact.SetError(contact_txt, "Enter contact details");
                disable_Add_btn();
            }
            else
            {
                eProvider_contact.Clear();
                enable_Add_Edit_btn();
            }
        }
        private void contact_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void email_txt_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(email_txt.Text, emailPattern) || string.IsNullOrEmpty(email_txt.Text))
            {
                email_txt.Focus();
                eProvider__email.SetError(email_txt, "Enter a valid email");
                disable_Add_btn();
            }
            else
            {
                eProvider__email.Clear();
                enable_Add_Edit_btn();
            }
        }

        private void regNo_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(regNo_txt.Text))
            {
                regNo_txt.Focus();
                eProvider_RegNo.SetError(regNo_txt, "Enter registration number");
                disable_Add_btn();
            }
            else
            {
                eProvider_RegNo.Clear();
                enable_Add_Edit_btn();
            }
        }





        #endregion

        #region Button Clicks

        private void addStu_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile 'Adding Student'";
          
            try
            {
                //MessageBox.Show(Is_ValidData().ToString());
                if (Is_ValidData())
                {
                    if (!Check_DuplicateNames(string.Concat(F_Name_txt.Text.Trim(), " ", L_Name_txt.Text.Trim())))
                    {
                        SqlCommand cmd_person = new SqlCommand("INSERT INTO Person values(@FirstName,@LastName, @Contact,@Email,@DateOfBirth,@Gender)", con);
                        cmd_person.Parameters.AddWithValue("@FirstName", F_Name_txt.Text);
                        cmd_person.Parameters.AddWithValue("@LastName", L_Name_txt.Text);
                        cmd_person.Parameters.AddWithValue("@Contact", contact_txt.Text);
                        cmd_person.Parameters.AddWithValue("@Email", email_txt.Text);
                        cmd_person.Parameters.AddWithValue("@DateOfBirth", dob_picker.Value);
                        cmd_person.Parameters.AddWithValue("@Gender", getLookup_Details("Gender", gender_combox.SelectedItem.ToString(), "ID")); //Male


                        con.Open();
                        cmd_person.ExecuteNonQuery();
                        con.Close();


                        //Getting related person Id from db
                        int personId = getIdFormPersonTable(gender_combox.SelectedIndex + 1);

                        // DB Data insertion of Student table.
                        SqlCommand cmd_student = new SqlCommand("INSERT INTO Student values(@Id,@RegistrationNo)", con);
                        cmd_student.Parameters.AddWithValue("@Id", personId);
                        cmd_student.Parameters.AddWithValue("@RegistrationNo", regNo_txt.Text.ToString());


                        con.Open();
                        cmd_student.ExecuteNonQuery();
                        con.Close();

                        MessageBox.Show("Data added sucessfully!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        form_Clear();
                        bindData_DG(); 
                    }
                    else 
                    {
                       MessageBox.Show("Name has been already taken.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else 
                {
                    MessageBox.Show("Fill all the feilds", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception err) 
            {
                MessageBox.Show(err.ToString()+errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }

       
      

        private void editStu_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile editing data";
            try
            {
                if (Is_ValidData())
                {
                        SqlCommand cmd_person = new SqlCommand("UPDATE  Person set FirstName=@FirstName,LastName=@LastName, Contact=@Contact,Email=@Email,DateOfBirth=@DateOfBirth,Gender=@Gender WHERE Id=@Id ", con);
                        cmd_person.Parameters.AddWithValue("@FirstName", F_Name_txt.Text);
                        cmd_person.Parameters.AddWithValue("@LastName", L_Name_txt.Text);
                        cmd_person.Parameters.AddWithValue("@Contact", contact_txt.Text);
                        cmd_person.Parameters.AddWithValue("@Email", email_txt.Text);
                        cmd_person.Parameters.AddWithValue("@DateOfBirth", dob_picker.Value);
                        cmd_person.Parameters.AddWithValue("@Id", id_txt.Text);
                        cmd_person.Parameters.AddWithValue("@Gender", getLookup_Details("Gender", gender_combox.SelectedItem.ToString(), "ID")); //Male

                        con.Open();
                        cmd_person.ExecuteNonQuery();
                        con.Close();

                        //Saving data to student table
                        SqlCommand cmd_student = new SqlCommand("UPDATE Student Set RegistrationNo=@RegistrationNo  WHERE Id=@Id ", con);
                        cmd_student.Parameters.AddWithValue("@Id", id_txt.Text);
                        cmd_student.Parameters.AddWithValue("@RegistrationNo", regNo_txt.Text);

                        con.Open();
                        cmd_student.ExecuteNonQuery();
                        con.Close();

                        MessageBox.Show("Data edited sucessfully!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        form_Clear();
                        bindData_DG();


                        form_Clear();
                }
                else 
                {
                    MessageBox.Show("Fill all text feilds", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }


        private void search_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile searching data";
            try 
            {
                string cmd = "Select s.id,s.RegistrationNo,p.FirstName, p.LastName,p.Email,L.value,p.DateOfBirth,p.Contact  from Student s join Person p  on p.id=s.id join Lookup L on L.Id=P.Gender where FirstName like '%'+@FirstName+'%' " ;
                SqlDataAdapter sda = new SqlDataAdapter(cmd, con);
                sda.SelectCommand.Parameters.AddWithValue("@FirstName", search_box_txt.Text.Trim());
                DataTable data = new DataTable();
                sda.Fill(data);
                dataGridView_Students.DataSource = data;
                dataGridView_Students.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        #endregion

        #region Testing
        private void gender_combox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(getLookup_Details("Gender", gender_combox.SelectedItem.ToString(),"Id"));
        }
        #endregion

    }
}
