﻿using MID_Project_FYP_Management.Helper_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_updateGroup : UserControl
    {
        public UC_updateGroup()
        {
            InitializeComponent();
        }

        private void addMember_btn_Click(object sender, EventArgs e)
        {
            var uc =new UC_addGroupMember();
            UserControl_Helper.addUserControl(uc,panelForm_Parent);
        }
    }
}
