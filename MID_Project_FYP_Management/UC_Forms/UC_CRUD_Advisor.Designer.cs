﻿namespace MID_Project_FYP_Management.UC_Forms
{
    partial class UC_CRUD_Advisor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.search_btn = new Guna.UI2.WinForms.Guna2Button();
            this.search_box_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.F_Name_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.contact_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.eProvider_contact = new System.Windows.Forms.ErrorProvider(this.components);
            this.L_Name_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.email_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.dob_picker = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.gender_combox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.eProvider_Designation = new System.Windows.Forms.ErrorProvider(this.components);
            this.eProvider_Gender = new System.Windows.Forms.ErrorProvider(this.components);
            this.eProvider__email = new System.Windows.Forms.ErrorProvider(this.components);
            this.eProvider_F_name = new System.Windows.Forms.ErrorProvider(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.salary_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.eProvider_L_name = new System.Windows.Forms.ErrorProvider(this.components);
            this.addAdvisor_btn = new Guna.UI2.WinForms.Guna2Button();
            this.editAdvisor_btn = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.designation_Combobox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.eProvider_salary = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridView_Advisor = new Guna.UI2.WinForms.Guna2DataGridView();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_contact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_Designation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_Gender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider__email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_F_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_L_name)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_salary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Advisor)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 312F));
            this.tableLayoutPanel5.Controls.Add(this.search_btn, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.search_box_txt, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 288);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1031, 40);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // search_btn
            // 
            this.search_btn.BorderColor = System.Drawing.Color.Blue;
            this.search_btn.BorderRadius = 5;
            this.search_btn.BorderThickness = 1;
            this.search_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.search_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.search_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.search_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.search_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.search_btn.FillColor = System.Drawing.Color.White;
            this.search_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search_btn.ForeColor = System.Drawing.Color.Black;
            this.search_btn.Location = new System.Drawing.Point(888, 3);
            this.search_btn.Name = "search_btn";
            this.search_btn.Size = new System.Drawing.Size(140, 34);
            this.search_btn.TabIndex = 8;
            this.search_btn.Text = "Search";
            this.search_btn.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // search_box_txt
            // 
            this.search_box_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.search_box_txt.DefaultText = "";
            this.search_box_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.search_box_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.search_box_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.search_box_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.search_box_txt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.search_box_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.search_box_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.search_box_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.search_box_txt.Location = new System.Drawing.Point(3, 3);
            this.search_box_txt.Name = "search_box_txt";
            this.search_box_txt.PasswordChar = '\0';
            this.search_box_txt.PlaceholderText = "Search By Name";
            this.search_box_txt.SelectedText = "";
            this.search_box_txt.Size = new System.Drawing.Size(713, 34);
            this.search_box_txt.TabIndex = 7;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(61, 40);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1043, 55);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Nirmala UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label7.Location = new System.Drawing.Point(350, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(341, 55);
            this.label7.TabIndex = 1;
            this.label7.Text = "Manage Advisors";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(697, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(515, 20);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label4.Size = new System.Drawing.Size(77, 15);
            this.label4.TabIndex = 41;
            this.label4.Text = "Last Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 20);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(77, 15);
            this.label2.TabIndex = 39;
            this.label2.Text = "First Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // F_Name_txt
            // 
            this.F_Name_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.F_Name_txt.DefaultText = "";
            this.F_Name_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.F_Name_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.F_Name_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.F_Name_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.F_Name_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.F_Name_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.F_Name_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.F_Name_txt.Location = new System.Drawing.Point(208, 3);
            this.F_Name_txt.Name = "F_Name_txt";
            this.F_Name_txt.PasswordChar = '\0';
            this.F_Name_txt.PlaceholderText = "First Name";
            this.F_Name_txt.SelectedText = "";
            this.F_Name_txt.Size = new System.Drawing.Size(174, 30);
            this.F_Name_txt.TabIndex = 23;
            this.F_Name_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.F_Name_txt_KeyPress);
            this.F_Name_txt.Leave += new System.EventHandler(this.F_Name_txt_Leave);
            // 
            // contact_txt
            // 
            this.contact_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.contact_txt.DefaultText = "";
            this.contact_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.contact_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.contact_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.contact_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.contact_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.contact_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.contact_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.contact_txt.Location = new System.Drawing.Point(208, 59);
            this.contact_txt.MaxLength = 11;
            this.contact_txt.Name = "contact_txt";
            this.contact_txt.PasswordChar = '\0';
            this.contact_txt.PlaceholderText = "Contact Number";
            this.contact_txt.SelectedText = "";
            this.contact_txt.Size = new System.Drawing.Size(174, 30);
            this.contact_txt.TabIndex = 24;
            this.contact_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.contact_txt_KeyPress);
            this.contact_txt.Leave += new System.EventHandler(this.contact_txt_Leave);
            // 
            // eProvider_contact
            // 
            this.eProvider_contact.ContainerControl = this;
            // 
            // L_Name_txt
            // 
            this.L_Name_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.L_Name_txt.DefaultText = "";
            this.L_Name_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.L_Name_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.L_Name_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.L_Name_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.L_Name_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.L_Name_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.L_Name_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.L_Name_txt.Location = new System.Drawing.Point(720, 3);
            this.L_Name_txt.Name = "L_Name_txt";
            this.L_Name_txt.PasswordChar = '\0';
            this.L_Name_txt.PlaceholderText = "Last Name";
            this.L_Name_txt.SelectedText = "";
            this.L_Name_txt.Size = new System.Drawing.Size(174, 30);
            this.L_Name_txt.TabIndex = 26;
            this.L_Name_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.L_Name_txt_KeyPress);
            this.L_Name_txt.Leave += new System.EventHandler(this.L_Name_txt_Leave);
            // 
            // email_txt
            // 
            this.email_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.email_txt.DefaultText = "";
            this.email_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.email_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.email_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.email_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.email_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.email_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.email_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.email_txt.Location = new System.Drawing.Point(720, 59);
            this.email_txt.Name = "email_txt";
            this.email_txt.PasswordChar = '\0';
            this.email_txt.PlaceholderText = "abc@gmail.com";
            this.email_txt.SelectedText = "";
            this.email_txt.Size = new System.Drawing.Size(174, 30);
            this.email_txt.TabIndex = 25;
            this.email_txt.Leave += new System.EventHandler(this.email_txt_Leave);
            // 
            // dob_picker
            // 
            this.dob_picker.Checked = true;
            this.dob_picker.FillColor = System.Drawing.Color.Yellow;
            this.dob_picker.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dob_picker.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dob_picker.Location = new System.Drawing.Point(208, 115);
            this.dob_picker.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dob_picker.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dob_picker.Name = "dob_picker";
            this.dob_picker.Size = new System.Drawing.Size(174, 30);
            this.dob_picker.TabIndex = 36;
            this.dob_picker.Value = new System.DateTime(2023, 2, 26, 17, 48, 52, 232);
            // 
            // gender_combox
            // 
            this.gender_combox.BackColor = System.Drawing.Color.Transparent;
            this.gender_combox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.gender_combox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gender_combox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.gender_combox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.gender_combox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gender_combox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.gender_combox.ItemHeight = 30;
            this.gender_combox.Location = new System.Drawing.Point(720, 115);
            this.gender_combox.Name = "gender_combox";
            this.gender_combox.Size = new System.Drawing.Size(174, 36);
            this.gender_combox.TabIndex = 37;
            this.gender_combox.SelectedIndexChanged += new System.EventHandler(this.gender_combox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(515, 76);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(49, 15);
            this.label3.TabIndex = 40;
            this.label3.Text = "Email";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 76);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 38;
            this.label1.Text = "Contact N0";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // eProvider_Designation
            // 
            this.eProvider_Designation.ContainerControl = this;
            // 
            // eProvider_Gender
            // 
            this.eProvider_Gender.ContainerControl = this;
            // 
            // eProvider__email
            // 
            this.eProvider__email.ContainerControl = this;
            // 
            // eProvider_F_name
            // 
            this.eProvider_F_name.ContainerControl = this;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(515, 132);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label5.Size = new System.Drawing.Size(58, 15);
            this.label5.TabIndex = 42;
            this.label5.Text = "Gender";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 132);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label6.Size = new System.Drawing.Size(43, 15);
            this.label6.TabIndex = 43;
            this.label6.Text = "DOB";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 189);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label8.Size = new System.Drawing.Size(83, 15);
            this.label8.TabIndex = 44;
            this.label8.Text = "Designation";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(515, 189);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label11.Size = new System.Drawing.Size(52, 15);
            this.label11.TabIndex = 46;
            this.label11.Text = "Salary";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // salary_txt
            // 
            this.salary_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.salary_txt.DefaultText = "";
            this.salary_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.salary_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.salary_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.salary_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.salary_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.salary_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.salary_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.salary_txt.Location = new System.Drawing.Point(720, 171);
            this.salary_txt.Name = "salary_txt";
            this.salary_txt.PasswordChar = '\0';
            this.salary_txt.PlaceholderText = "Salary";
            this.salary_txt.SelectedText = "";
            this.salary_txt.Size = new System.Drawing.Size(174, 30);
            this.salary_txt.TabIndex = 47;
            this.salary_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.salary_txt_KeyPress);
            this.salary_txt.Leave += new System.EventHandler(this.salary_txt_Leave);
            // 
            // eProvider_L_name
            // 
            this.eProvider_L_name.ContainerControl = this;
            // 
            // addAdvisor_btn
            // 
            this.addAdvisor_btn.BorderColor = System.Drawing.Color.Blue;
            this.addAdvisor_btn.BorderRadius = 5;
            this.addAdvisor_btn.BorderThickness = 1;
            this.addAdvisor_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.addAdvisor_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.addAdvisor_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.addAdvisor_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.addAdvisor_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.addAdvisor_btn.FillColor = System.Drawing.Color.White;
            this.addAdvisor_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAdvisor_btn.ForeColor = System.Drawing.Color.Black;
            this.addAdvisor_btn.Location = new System.Drawing.Point(267, 3);
            this.addAdvisor_btn.Name = "addAdvisor_btn";
            this.addAdvisor_btn.Size = new System.Drawing.Size(140, 35);
            this.addAdvisor_btn.TabIndex = 9;
            this.addAdvisor_btn.Text = "+ Add Advisor";
            this.addAdvisor_btn.Click += new System.EventHandler(this.addAdvisor_btn_Click_1);
            // 
            // editAdvisor_btn
            // 
            this.editAdvisor_btn.BorderColor = System.Drawing.Color.Blue;
            this.editAdvisor_btn.BorderRadius = 5;
            this.editAdvisor_btn.BorderThickness = 1;
            this.editAdvisor_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.editAdvisor_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.editAdvisor_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.editAdvisor_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.editAdvisor_btn.Dock = System.Windows.Forms.DockStyle.Left;
            this.editAdvisor_btn.FillColor = System.Drawing.Color.White;
            this.editAdvisor_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editAdvisor_btn.ForeColor = System.Drawing.Color.Black;
            this.editAdvisor_btn.Location = new System.Drawing.Point(515, 3);
            this.editAdvisor_btn.Name = "editAdvisor_btn";
            this.editAdvisor_btn.Size = new System.Drawing.Size(140, 35);
            this.editAdvisor_btn.TabIndex = 10;
            this.editAdvisor_btn.Text = "Edit Advisor";
            this.editAdvisor_btn.Click += new System.EventHandler(this.editAdvisor_btn_Click_1);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.95122F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.04878F));
            this.tableLayoutPanel6.Controls.Add(this.addAdvisor_btn, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.editAdvisor_btn, 2, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 235);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1025, 41);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.46774F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.53226F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1031, 279);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 6;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel7.Controls.Add(this.designation_Combobox, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.F_Name_txt, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.contact_txt, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.L_Name_txt, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.email_txt, 4, 1);
            this.tableLayoutPanel7.Controls.Add(this.dob_picker, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.gender_combox, 4, 2);
            this.tableLayoutPanel7.Controls.Add(this.label3, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label5, 3, 2);
            this.tableLayoutPanel7.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.label11, 3, 3);
            this.tableLayoutPanel7.Controls.Add(this.salary_txt, 4, 3);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1025, 226);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // designation_Combobox
            // 
            this.designation_Combobox.BackColor = System.Drawing.Color.Transparent;
            this.designation_Combobox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.designation_Combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.designation_Combobox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.designation_Combobox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.designation_Combobox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.designation_Combobox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.designation_Combobox.ItemHeight = 30;
            this.designation_Combobox.Location = new System.Drawing.Point(208, 171);
            this.designation_Combobox.Name = "designation_Combobox";
            this.designation_Combobox.Size = new System.Drawing.Size(174, 36);
            this.designation_Combobox.TabIndex = 48;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.28763F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.71237F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1037, 331);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dataGridView_Advisor, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(61, 101);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.88723F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.11277F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1043, 603);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.18792F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.74496F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1166, 745);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // eProvider_salary
            // 
            this.eProvider_salary.ContainerControl = this;
            // 
            // dataGridView_Advisor
            // 
            this.dataGridView_Advisor.AllowUserToAddRows = false;
            this.dataGridView_Advisor.AllowUserToDeleteRows = false;
            this.dataGridView_Advisor.AllowUserToResizeColumns = false;
            this.dataGridView_Advisor.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView_Advisor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Advisor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_Advisor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_Advisor.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_Advisor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_Advisor.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_Advisor.Location = new System.Drawing.Point(3, 340);
            this.dataGridView_Advisor.Name = "dataGridView_Advisor";
            this.dataGridView_Advisor.RowHeadersVisible = false;
            this.dataGridView_Advisor.Size = new System.Drawing.Size(1037, 260);
            this.dataGridView_Advisor.TabIndex = 1;
            this.dataGridView_Advisor.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_Advisor.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dataGridView_Advisor.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dataGridView_Advisor.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dataGridView_Advisor.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dataGridView_Advisor.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_Advisor.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_Advisor.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.dataGridView_Advisor.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView_Advisor.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_Advisor.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dataGridView_Advisor.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Advisor.ThemeStyle.HeaderStyle.Height = 4;
            this.dataGridView_Advisor.ThemeStyle.ReadOnly = false;
            this.dataGridView_Advisor.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_Advisor.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView_Advisor.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_Advisor.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_Advisor.ThemeStyle.RowsStyle.Height = 22;
            this.dataGridView_Advisor.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_Advisor.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_Advisor.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Advisor_CellClick);
            // 
            // UC_CRUD_Advisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UC_CRUD_Advisor";
            this.Size = new System.Drawing.Size(1166, 745);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_contact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_Designation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_Gender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider__email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_F_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_L_name)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.eProvider_salary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Advisor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private Guna.UI2.WinForms.Guna2Button search_btn;
        private Guna.UI2.WinForms.Guna2TextBox search_box_txt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox F_Name_txt;
        private Guna.UI2.WinForms.Guna2TextBox contact_txt;
        private System.Windows.Forms.ErrorProvider eProvider_contact;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private Guna.UI2.WinForms.Guna2Button addAdvisor_btn;
        private Guna.UI2.WinForms.Guna2Button editAdvisor_btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private Guna.UI2.WinForms.Guna2TextBox L_Name_txt;
        private Guna.UI2.WinForms.Guna2TextBox email_txt;
        private Guna.UI2.WinForms.Guna2DateTimePicker dob_picker;
        private Guna.UI2.WinForms.Guna2ComboBox gender_combox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private Guna.UI2.WinForms.Guna2TextBox salary_txt;
        private System.Windows.Forms.ErrorProvider eProvider_Designation;
        private System.Windows.Forms.ErrorProvider eProvider_Gender;
        private System.Windows.Forms.ErrorProvider eProvider__email;
        private System.Windows.Forms.ErrorProvider eProvider_F_name;
        private System.Windows.Forms.ErrorProvider eProvider_L_name;
        private Guna.UI2.WinForms.Guna2ComboBox designation_Combobox;
        private System.Windows.Forms.ErrorProvider eProvider_salary;
        private Guna.UI2.WinForms.Guna2DataGridView dataGridView_Advisor;
    }
}
