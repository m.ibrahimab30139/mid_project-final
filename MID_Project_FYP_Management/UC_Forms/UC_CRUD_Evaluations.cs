﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_CRUD_Evaluations : UserControl
    {
        #region variables
        static readonly string cs = ConfigurationManager.ConnectionStrings["dbProjectA"].ConnectionString;
        readonly SqlConnection con = new SqlConnection(cs);

        #endregion

        public UC_CRUD_Evaluations()
        {
            InitializeComponent();
            bindData_DG();
            evaluationName_txt.Focus();
        }

        #region Button Clicks
        private void search_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile searching data";
            try
            {
                string cmd = "SELECT * FROM Evaluation WHERE Name like '%'+@Name+'%' ";
                SqlDataAdapter sda = new SqlDataAdapter(cmd, con);
                sda.SelectCommand.Parameters.AddWithValue("@Name", search_box_txt.Text.Trim());
                DataTable data = new DataTable();
                sda.Fill(data);
                dataGridView_Evaluations.DataSource = data;
                dataGridView_Evaluations.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addEvaluation_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile 'Adding Evaluation'";

            try
            {
                if (Is_ValidData())
                {
                    MessageBox.Show(Is_ValidData().ToString());
                    if (!Check_DuplicateNames(evaluationName_txt.Text))
                    {
                        if (returnTotal_Weightage() + int.Parse(totalWeigtage_txt.Text) < 100)
                        {
                            SqlCommand cmd = new SqlCommand("INSERT INTO Evaluation values(@Name,@TotalMarks, @TotalWeightage)", con);
                            cmd.Parameters.AddWithValue("@Name", evaluationName_txt.Text);
                            cmd.Parameters.AddWithValue("@TotalMarks", totalMarks_txt.Text);
                            cmd.Parameters.AddWithValue("@TotalWeightage", totalWeigtage_txt.Text);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();



                            MessageBox.Show("Data added sucessfully!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            form_Clear();
                            bindData_DG();
                        }
                        else 
                        {
                            MessageBox.Show("Evaluations total weightage cannot exceed 100 %", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Evaluation Name has been already taken.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                }
                else
                {
                    MessageBox.Show("Invalid Evaluation data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        private void editEvaluation_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile editing evaluation";
            try
            {
                //MessageBox.Show(Is_ValidData().ToString());
                if (Is_ValidData())
                {
                    if (!Check_DuplicateNames(evaluationName_txt.Text))
                    {
                        SqlCommand cmd = new SqlCommand("UPDATE Evaluation set Name=@Name,TotalMarks=@TotalMarks, TotalWeightage=@TotalWeightage  WHERE Id=@Id ", con);
                        cmd.Parameters.AddWithValue("@Name", evaluationName_txt.Text);
                        cmd.Parameters.AddWithValue("@TotalMarks", totalMarks_txt.Text);
                        cmd.Parameters.AddWithValue("@TotalWeightage", totalWeigtage_txt.Text);

                        cmd.Parameters.AddWithValue("@Id", dataGridView_Evaluations.SelectedRows[0].Cells["Id"].Value);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        form_Clear();
                        bindData_DG();
                    }
                    else
                    {
                        MessageBox.Show("Evaluation Name has been already taken.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                }
                else
                {
                    MessageBox.Show("Invalid Evaluation data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Helper Functions
        private bool Check_DuplicateNames(string name)
        {
            string errorMessege = "\nWhile 'checking name duplicates'";

            string query = $" SELECT ID " +
                $" FROM Evaluation " +
                $" WHERE " +
                $" Name = '{name}' ";
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                var e_Id = cmd.ExecuteScalar();
                if (e_Id != null)
                {
                    if (int.TryParse(e_Id.ToString(), out int Id))
                    {
                        MessageBox.Show(Id.ToString());
                        con.Close();
                        return true;
                    }

                }
                else
                {
                    con.Close();
                    return false;
                }
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }

            return true;

        }
        private void form_Clear()
        {
            evaluationName_txt.Text = "";
            totalWeigtage_txt.Text = "";
            totalMarks_txt.Text = "";
        }


        #endregion

        #region Data grid view functions 
        private void bindData_DG()
        {
            dataGridView_Evaluations.ColumnHeadersHeight = 25;
            string errorMessege = "\nWhile Binding evaluation";
            try
            {
                string query = "SELECT * FROM Evaluation";
                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Evaluations.DataSource = dt;
                dataGridView_Evaluations.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void dataGridView_Evaluations_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            evaluationName_txt.Text = dataGridView_Evaluations.SelectedRows[0].Cells["Name"].Value.ToString();
            totalMarks_txt.Text = dataGridView_Evaluations.SelectedRows[0].Cells["TotalMarks"].Value.ToString();
            totalWeigtage_txt.Text = dataGridView_Evaluations.SelectedRows[0].Cells["TotalWeightage"].Value.ToString();
        }
        #endregion

        #region Databse attribute getting functions
        private int returnTotal_Weightage()
        {
            string errorMessege = "\nWhile 'calculating total weightage'";
            int weightage = 0; //For error purposes

            string query = $"SELECT SUM(TotalWeightage) FROM Evaluation";
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                weightage = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            //MessageBox.Show(id.ToString());
            return weightage;
        }

      
        #endregion

        #region Data Validations
        private bool Is_ValidData()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(evaluationName_txt.Text)) { return false; }
            if (string.IsNullOrEmpty(totalMarks_txt.Text)) { return false; }
            if (string.IsNullOrEmpty(totalWeigtage_txt.Text)) { return false; }
            if (int.Parse(totalWeigtage_txt.Text.ToString()) > 100) { return false; }
            return isValid;
        }
        private void evaluationName_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(evaluationName_txt.Text)) 
            {
                eProvider_EvaluationName.SetError(evaluationName_txt, "Enter evaluation name");
                evaluationName_txt.Focus();
            }
            else 
            {
                eProvider_EvaluationName.Clear();
            }
        }
        private void totalMarks_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(totalMarks_txt.Text))
            {
                eProvider_TotalMarks.SetError(totalMarks_txt, "Enter total marks for evaluation");
                totalMarks_txt.Focus();
            }
           
            else 
            {
                eProvider_TotalMarks.Clear();
            }
        }
        private void totalMarks_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void totalWeigtage_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void totalWeigtage_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(totalWeigtage_txt.Text))
            {
                eProvider_TotalWeigtage.SetError(totalWeigtage_txt, "Enter weightage of evaluation");
                totalWeigtage_txt.Focus();
            }
            else 
            {
                if (int.Parse(totalWeigtage_txt.Text.ToString()) > 100)
                {
                    eProvider_TotalWeigtage.SetError(totalWeigtage_txt, "Weightage of evluation cannot be more than 100 %");
                    totalWeigtage_txt.Focus();
                }
                else
                {
                    eProvider_TotalWeigtage.Clear();
                }

            }
           
            
        }

        #endregion

    }
}
