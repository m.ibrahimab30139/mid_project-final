﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_EvaluateGroup : UserControl
    {
        #region Variables
        static readonly string cs = ConfigurationManager.ConnectionStrings["dbProjectA"].ConnectionString;
        readonly SqlConnection con = new SqlConnection(cs);
        #endregion
        public UC_EvaluateGroup()
        {
            InitializeComponent();
            fill_EvaluationName_Combobox();
        }
        #region Data filling Functions

        private void fill_EvaluationName_Combobox() 
        {
            string errorMessege = "\nWhile binding evaluation names";
            try
            {
                string query = "    SELECT E.Name" +
                                "   FROM[ProjectA].[dbo].[Evaluation] E";
                SqlCommand sqlCommand = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    string item = dataReader.GetString(0); //Because value is at 1 index
                    evaluationName_combobox.Items.Add(item);
                }
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        private void evaluationName_combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string evaluationName= evaluationName_combobox.SelectedItem.ToString();
            string marks = get_Evaluation_Details(evaluationName, "TotalMarks");
            string totalWeightage = get_Evaluation_Details(evaluationName, "TotalWeightage");

            totalMarks_txt.Text = marks;
            totalWeightage_txt.Text = totalWeightage;

            //string evaluationId = get_Evaluation_Details(evaluationName_combobox.SelectedItem.ToString(), "Id");
            //MessageBox.Show(evaluationId);

            bind_UnevaluatedGroups(evaluationName);
            bind_EvaluatedGroups(evaluationName);

            dataGridView_Evaluated_Groups_Details.DataSource=null;
            dataGridView_Unevaluated_Groups_Details.DataSource = null;
            groupId_txt.Text = "";
        }
        private string get_Evaluation_Details(string evaluationName, string attribute)
        {
            string errorMessege = $"\nWhile getting {attribute} from evaluation";
            string output = "";
            string query = $"  SELECT {attribute}" +
                            $" FROM EVALUATION" +
                            $" WHERE Name='{evaluationName}'";
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                output = cmd.ExecuteScalar().ToString();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return output;
        }

        #endregion

        #region Datagrid View Functions
        private void bind_UnevaluatedGroups(string evaluationName)
        {
            string errorMessege = "\nWhile binding unevaluated groups";
            try
            {
                string query = $"SELECT * " +
                    $"FROM [ProjectA].[dbo].[Group]G " +
                    $"WHERE G.Id NOT IN " +
                    $"  ( SELECT GroupId  FROM GroupEvaluation GE JOIN Evaluation E ON E.Id=GE.EvaluationId WHERE E.Name='{evaluationName}' ) " +
                    $"AND G.Id IN" +
                    $"  ( SELECT GP.GroupId FROM GroupProject GP)";

                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Unevaluated_Groups.DataSource = dt;
                dataGridView_Unevaluated_Groups.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        private void dataGridView_Unevaluated_Groups_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            groupId_txt.Text = dataGridView_Unevaluated_Groups.SelectedRows[0].Cells["Id"].Value.ToString();
            MessageBox.Show(groupId_txt.Text);
            bind_UnevaluatedGroups_Details(groupId_txt.Text);
        }

        private void bind_UnevaluatedGroups_Details(string groupId)
        {
            string errorMessege = $"\nWhile binding un_evaluated group {groupId} details";
            try
            {
                string query = $"SELECT S.RegistrationNo, CONCAT(P.FirstName,' ',P.LastName) 'Name',L.Value 'Status'" +
                    $" FROM [ProjectA].[dbo].[GroupStudent]GS" +
                    $" JOIN Student S ON S.Id=GS.StudentId" +
                    $" JOIN Person P ON S.Id=P.Id" +
                    $" JOIN Lookup L ON L.Id=GS.Status" +
                    $" WHERE GroupId={groupId} AND L.Value='Active'";

                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Unevaluated_Groups_Details.DataSource = dt;
                dataGridView_Unevaluated_Groups_Details.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }

        private void bind_EvaluatedGroups(string evaluationName)
        {
            string errorMessege = "\nWhile binding evaluated groups";
            try
            {
                string query = $"SELECT * " +
                    $"FROM [ProjectA].[dbo].[Group]G " +
                    $"WHERE G.Id IN " +
                    $"  ( SELECT GroupId  FROM GroupEvaluation GE JOIN Evaluation E ON E.Id=GE.EvaluationId WHERE E.Name='{evaluationName}' ) " +
                    $"AND G.Id IN" +
                    $"  ( SELECT GP.GroupId FROM GroupProject GP)";

                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Evaluated_Groups.DataSource = dt;
                dataGridView_Evaluated_Groups.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        private void dataGridView_Evaluated_Groups_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string groupId= dataGridView_Evaluated_Groups.SelectedRows[0].Cells["Id"].Value.ToString();
            string evaluationId=get_Evaluation_Details(evaluationName_combobox.SelectedItem.ToString(), "Id");
            bind_EvaluatedGroups_Details(groupId, evaluationId);
        }

        private void bind_EvaluatedGroups_Details(string groupId, string EvaluationId)
        {
            string errorMessege = "\nWhile binding evaluated groups";
            try
            {
                string query = $"SELECT GE.GroupId,GE.ObtainedMarks, E.TotalMarks,E.TotalWeightage,GE.EvaluationDate\r\n" +
                    $" FROM GroupEvaluation GE" +
                    $" JOIN Evaluation E ON GE.EvaluationId=E.Id" +
                    $" WHERE GE.GroupId={groupId} AND GE.EvaluationId={EvaluationId}";

                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Evaluated_Groups_Details.DataSource = dt;
                dataGridView_Evaluated_Groups_Details.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }


        #endregion



        #region Data Validations

        private bool Is_ValidData()
        {
            bool isValid = true;
            if (evaluationName_combobox.SelectedIndex == -1) { isValid = false; }
            if (string.IsNullOrEmpty(marksObtained_txt.Text)) { isValid = false; }
            if (string.IsNullOrEmpty(groupId_txt.Text)) { isValid = false; }
            if (Convert.ToInt32(marksObtained_txt.Text) > Convert.ToInt32(totalMarks_txt.Text))
            { isValid = false; }
            return isValid;
        }
        private void marksObtained_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void marksObtained_txt_Leave(object sender, EventArgs e)
        {
            if (Convert.ToInt32(marksObtained_txt.Text) > Convert.ToInt32(totalMarks_txt.Text))
            {
                eProvider_MarksObtained.SetError(marksObtained_txt, "Obtained marks should be less than total marks");
            }
            else 
            {
                eProvider_MarksObtained.Clear();
            }
        }

        #endregion

        #region Button Clicks
        private void evaluateGroup_btn_Click(object sender, EventArgs e)
        {
            //Evaluation id, Group Id, Obtained marks , Evaluation date
            string groupId = groupId_txt.Text;
            string evaluationId = get_Evaluation_Details(evaluationName_combobox.SelectedItem.ToString(), "Id");
            //MessageBox.Show(evaluationId);
            string errorMessege = "\nWhile evaluating group";
            try
            {
                if (Is_ValidData())
                {

                    string query = $"INSERT INTO GroupEvaluation values(@GroupId,@EvaluationId,@ObtainedMarks,@EvaluationDate)";

                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@GroupId", groupId);
                    cmd.Parameters.AddWithValue("@EvaluationId", evaluationId);
                    cmd.Parameters.AddWithValue("@ObtainedMarks", marksObtained_txt.Text);
                    cmd.Parameters.AddWithValue("@EvaluationDate", DateTime.Now.ToString());

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show($"Group {groupId} has been evaluated", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    bind_EvaluatedGroups(evaluationName_combobox.SelectedItem.ToString());
                    bind_UnevaluatedGroups(evaluationName_combobox.SelectedItem.ToString());
                }
                else
                {
                    MessageBox.Show("Enter valid data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        #endregion
        
    }
}
