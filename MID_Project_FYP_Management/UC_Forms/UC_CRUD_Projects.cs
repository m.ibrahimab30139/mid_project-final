﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_CRUD_Projects : UserControl
    {
        #region Variables
        static readonly string cs = ConfigurationManager.ConnectionStrings["dbProjectA"].ConnectionString;
        readonly SqlConnection con = new SqlConnection(cs);
        string projectId_DG = "";

        #endregion

        public UC_CRUD_Projects()
        {
            InitializeComponent();

            fill_Advisors_Combobox();
            bind_Projects_DG();
            dataGridView_Projects.ColumnHeadersHeight = 20;
            dataGridView_Project_Advisors.ColumnHeadersHeight = 20;

        }

        #region Formating functions
        private void form_Clear()
        {
            title_txt.Text = "";
            description_txt.Text = "";
            co_advisor_combobox.SelectedIndex = -1;
            mainAdvisor_combobox.SelectedIndex = -1;
            industryAdvisor_combox.SelectedIndex = -1;
        }

        #endregion

        #region Data base related functions
        private int getIdFromTable()
        {

            string errorMessege = "\nWhile 'getting Project Id'";
            int id = -1; //For error purposes

            string query = $" select id " +
                $" from Project " +
                $" where " +
                $" Title = '{title_txt.Text}' and " +
                $" Description = '{description_txt.Text}'";

            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }

            return id;
        }
        private string getLookup_Details(string value, string requiredAttribute)
        {
            string id = "";
            string errorMessege = $"\nWhile geting {requiredAttribute} from lookupTable";
            try
            {
                string query = $"SELECT {requiredAttribute} FROM Lookup WHERE Lookup.Value='{value}'";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                id = cmd.ExecuteScalar().ToString();
                con.Close();
                return id;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return id;
        }
        private int getAdvisorId(string name)
        {
            string[] advisorName = name.Split(' ');
            string f_Name = advisorName[0];
            string l_name = advisorName[1];
            string errorMessege = "\nWhile 'getting Advisor Id'";
            int id = -1; //For error purposes

            string query = $" select id " +
                $" from Person " +
                $" where " +
                $" FirstName = '{f_Name}' and " +
                $" LastName = '{l_name}'";


            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            //MessageBox.Show(id.ToString());
            return id;
        }
        #endregion

        #region Data Filling Functions

        private void fill_Advisors_Combobox() 
        {
            string errorMessege = "\nWhile filling advisors comboboxes";
            try
            {
                string query =
                    "Select  p.firstname+' '+p.lastname from Advisor a join person p on p.id = a.id"; //join multiple table person and student;
                SqlCommand sqlCommand = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    string item = dataReader.GetString(0); //Because value is at 1 index
                    mainAdvisor_combobox.Items.Add(item);
                    co_advisor_combobox.Items.Add(item);

                }
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }

            try
            {

                string query1 =
                   "Select  p.firstname+' '+p.lastname from Advisor a join person p on p.id = a.id join lookup l on l.id=a.designation where l.value='Industry Professional'"; //join multiple table person and student;
                SqlCommand sqlCommand1 = new SqlCommand(query1, con);
                con.Open();
                SqlDataReader dataReader1 = sqlCommand1.ExecuteReader();
                while (dataReader1.Read())
                {
                    string item1 = dataReader1.GetString(0); //Because value is at 1 index
                    industryAdvisor_combox.Items.Add(item1);
                }
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }

        private void fill_PA_Comboboxes(string pId)
        {
                string errorMessege1 = "\nWhile filling main advisor combobox";
            string query1 =
            $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Advisor Name', L.Value 'Advisor Role', PA.AssignmentDate" +
            $"  FROM  ProjectAdvisor PA" +
            $"  JOIN Lookup L  ON L.Id=PA.AdvisorRole" +
            $"  JOIN Person P  ON P.Id=PA.AdvisorId" +
            $"  WHERE PA.ProjectId={pId} AND PA.AdvisorRole=11";

            SqlCommand cmd1 = new SqlCommand(query1, con);
            try
            {
                con.Open();
                mainAdvisor_combobox.Text = cmd1.ExecuteScalar().ToString();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege1, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            string errorMessege2 = "\nWhile filling co-advisor advisor combobox";


            string query2 =
            $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Advisor Name', L.Value 'Advisor Role', PA.AssignmentDate" +
            $"  FROM  ProjectAdvisor PA" +
            $"  JOIN Lookup L  ON L.Id=PA.AdvisorRole" +
            $"  JOIN Person P  ON P.Id=PA.AdvisorId" +
            $"  WHERE PA.ProjectId={pId} AND PA.AdvisorRole=12";

            SqlCommand cmd2 = new SqlCommand(query2, con);
            try
            {
                con.Open();
                co_advisor_combobox.Text = cmd2.ExecuteScalar().ToString();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }

            string errorMessege3 = "\nWhile filling main advisor combobox";

            string query3 =
            $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Advisor Name', L.Value 'Advisor Role', PA.AssignmentDate" +
            $"  FROM  ProjectAdvisor PA" +
            $"  JOIN Lookup L  ON L.Id=PA.AdvisorRole" +
            $"  JOIN Person P  ON P.Id=PA.AdvisorId" +
            $"  WHERE PA.ProjectId={pId} AND PA.AdvisorRole=14";

            SqlCommand cmd3 = new SqlCommand(query3, con);
            try
            {
                con.Open();
                industryAdvisor_combox.Text = cmd3.ExecuteScalar().ToString();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege3, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }

        }

        #endregion

        #region Data grid view related functions

        private void bind_Projects_DG()
        {
            string errorMessege = "\nWhile binding projects datagridview";
            try
            {
                SqlCommand cmd = new SqlCommand(
                   "Select Id,Title,Description From Project", con); //join multiple table person and student

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Projects.DataSource = dt;
                dataGridView_Projects.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bind_PAdvisors_DG(string pId)
        {
            string query =
                $"SELECT CONCAT(P.FirstName,' ',P.LastName) 'Advisor Name', L.Value 'Advisor Role', PA.AssignmentDate" +
                $"  FROM  ProjectAdvisor PA" +
                $"  JOIN Lookup L  ON L.Id=PA.AdvisorRole" +
                $"  JOIN Person P  ON P.Id=PA.AdvisorId" +
                $"  WHERE PA.ProjectId={pId}";


            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView_Project_Advisors.DataSource = dt;
            dataGridView_Project_Advisors.Refresh();
        }
        private void dataGridView_Projects_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            projectId_DG = dataGridView_Projects.SelectedRows[0].Cells[0].Value.ToString();
            //1.Send data to text boxes 

            description_txt.Text = dataGridView_Projects.SelectedRows[0].Cells[2].Value.ToString();
            title_txt.Text = dataGridView_Projects.SelectedRows[0].Cells[1].Value.ToString();

            fill_PA_Comboboxes(projectId_DG);
            //2.Show the advisors and their role in dataGrid_View_Advisors
            bind_PAdvisors_DG(projectId_DG);
        }
        private bool Check_Duplicate(string projectName)
        {
            string errorMessege = "\nWhile 'checking name duplicates'";
            int id = -1; //For error purposes

            string query = $" SELECT ID FROM PROJECT WHERE Title={projectName}";
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            //MessageBox.Show(id.ToString());
            if (id == -1) { return false; }
            else { return true; }

        }
        #endregion

        #region Button Clicks
        private void addProject_btn_Click_1(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile adding project";
            try
            {
                //MessageBox.Show(Is_ValidData().ToString());
                if (Is_ValidData()==true || valid_Advisors()==true)
                {
                    //Check duplicates
                    if (!Check_Duplicate(title_txt.Text.ToString()))
                    {
                        SqlCommand cmd_project = new SqlCommand("INSERT INTO Project values(@Description,@Title)", con);
                        cmd_project.Parameters.AddWithValue("@Title", title_txt.Text);
                        cmd_project.Parameters.AddWithValue("@Description", description_txt.Text);

                        con.Open();
                        cmd_project.ExecuteNonQuery();
                        con.Close();

                        //Getting related person Id from db
                        int projectId = getIdFromTable();

                        MessageBox.Show(projectId.ToString());

                        // DB Data insertion of Student table.
                        //Main Advisor
                        SqlCommand cmd_Mainadvisor = new SqlCommand("INSERT INTO ProjectAdvisor values(@AdvisorId,@ProjectId,@AdvisorRole,@AssignmentDate)", con);
                        cmd_Mainadvisor.Parameters.AddWithValue("@AdvisorId", getAdvisorId(mainAdvisor_combobox.SelectedItem.ToString()));
                        cmd_Mainadvisor.Parameters.AddWithValue("@ProjectId", projectId);
                        cmd_Mainadvisor.Parameters.AddWithValue("@AdvisorRole", getLookup_Details("Main Advisor", "Id"));
                        cmd_Mainadvisor.Parameters.AddWithValue("@AssignmentDate", assignmentDate_picker.Value);
                        con.Open();
                        cmd_Mainadvisor.ExecuteNonQuery();
                        con.Close();

                        //.Show(getAdvisorId(mainAdvisor_combobox.SelectedItem.ToString()).ToString());

                        //Co-advisor
                        SqlCommand cmd_Co_advisor = new SqlCommand("INSERT INTO ProjectAdvisor values(@AdvisorId,@ProjectId,@AdvisorRole,@AssignmentDate)", con);
                        cmd_Co_advisor.Parameters.AddWithValue("@AdvisorId", getAdvisorId(co_advisor_combobox.SelectedItem.ToString()));
                        cmd_Co_advisor.Parameters.AddWithValue("@ProjectId", projectId);
                        cmd_Co_advisor.Parameters.AddWithValue("@AdvisorRole", getLookup_Details("Co-Advisror", "Id"));
                        cmd_Co_advisor.Parameters.AddWithValue("@AssignmentDate", assignmentDate_picker.Value);
                        con.Open();
                        cmd_Co_advisor.ExecuteNonQuery();
                        con.Close();

                        //MessageBox.Show(getAdvisorId(co_advisor_combobox.SelectedItem.ToString()).ToString());


                        //Industry advisor
                        SqlCommand cmd_Industry_advisor = new SqlCommand("INSERT INTO ProjectAdvisor values(@AdvisorId,@ProjectId,@AdvisorRole,@AssignmentDate)", con);
                        cmd_Industry_advisor.Parameters.AddWithValue("@AdvisorId", getAdvisorId((industryAdvisor_combox.SelectedItem.ToString().Trim())));
                        cmd_Industry_advisor.Parameters.AddWithValue("@ProjectId", projectId);
                        cmd_Industry_advisor.Parameters.AddWithValue("@AdvisorRole", getLookup_Details("Industry Advisor", "Id"));
                        cmd_Industry_advisor.Parameters.AddWithValue("@AssignmentDate", assignmentDate_picker.Value);
                        con.Open();
                        cmd_Industry_advisor.ExecuteNonQuery();
                        con.Close();

                        // MessageBox.Show(getAdvisorId(industryAdvisor_combox.SelectedItem.ToString()).ToString());


                        MessageBox.Show("Data added sucessfully!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        form_Clear();
                        bind_Projects_DG();
                    }
                    else 
                    {
                        MessageBox.Show("Project title has already been taken", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        form_Clear();
                    }
                }
                else
                {
                    MessageBox.Show("Fill all feilds correctly or Two advisors cannot be same", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool valid_Advisors()
        {
            bool isValid = true;
            if (mainAdvisor_combobox.SelectedItem == co_advisor_combobox.SelectedItem) { isValid = false; }
            if (string.Equals(mainAdvisor_combobox.SelectedItem, industryAdvisor_combox.SelectedItem)) { isValid = false; }
            if(string.Equals(co_advisor_combobox.SelectedItem, industryAdvisor_combox.SelectedItem)) { isValid = false; }

            return isValid;
        }

        private void editProject_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile editting project";
            try
            {
                //MessageBox.Show(Is_ValidData().ToString());
                if (Is_ValidData() == true && valid_Advisors() == true)
                {
                    SqlCommand cmd_project = new SqlCommand("UPDATE Project SET Description=@Description,Title=@Title WHERE ID=@ID", con);
                    cmd_project.Parameters.AddWithValue("@Title", title_txt.Text);
                    cmd_project.Parameters.AddWithValue("@Description", description_txt.Text);
                    cmd_project.Parameters.AddWithValue("@ID", projectId_DG.ToString());

                    con.Open();
                    cmd_project.ExecuteNonQuery();
                    con.Close();


                    // DB Data insertion of Student table.
                    //Main Advisor
                    SqlCommand cmd_Mainadvisor = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorId=@AdvisorId,AdvisorRole=@AdvisorRole,AssignmentDate=@AssignmentDate WHERE ProjectId=@ProjectId", con);
                    cmd_Mainadvisor.Parameters.AddWithValue("@AdvisorId", getAdvisorId(mainAdvisor_combobox.SelectedItem.ToString()));
                    cmd_Mainadvisor.Parameters.AddWithValue("@ProjectId", projectId_DG);
                    cmd_Mainadvisor.Parameters.AddWithValue("@AdvisorRole", 11);
                    cmd_Mainadvisor.Parameters.AddWithValue("@AssignmentDate", assignmentDate_picker.Value);
                    con.Open();
                    cmd_Mainadvisor.ExecuteNonQuery();
                    con.Close();

                    //.Show(getAdvisorId(mainAdvisor_combobox.SelectedItem.ToString()).ToString());

                    //Co-advisor
                    SqlCommand cmd_Co_advisor = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorId=@AdvisorId,AdvisorRole=@AdvisorRole,AssignmentDate=@AssignmentDate WHERE ProjectId=@ProjectId", con);
                    cmd_Co_advisor.Parameters.AddWithValue("@AdvisorId", getAdvisorId(co_advisor_combobox.SelectedItem.ToString()));
                    cmd_Co_advisor.Parameters.AddWithValue("@ProjectId", projectId_DG);
                    cmd_Co_advisor.Parameters.AddWithValue("@AdvisorRole", 12);
                    cmd_Co_advisor.Parameters.AddWithValue("@AssignmentDate", assignmentDate_picker.Value);
                    con.Open();
                    cmd_Co_advisor.ExecuteNonQuery();
                    con.Close();

                    //MessageBox.Show(getAdvisorId(co_advisor_combobox.SelectedItem.ToString()).ToString());


                    //Industry advisor
                    SqlCommand cmd_Industry_advisor = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorId=@AdvisorId,AdvisorRole=@AdvisorRole,AssignmentDate=@AssignmentDate WHERE ProjectId=@ProjectId", con);
                    cmd_Industry_advisor.Parameters.AddWithValue("@AdvisorId", getAdvisorId(industryAdvisor_combox.SelectedItem.ToString()));
                    cmd_Industry_advisor.Parameters.AddWithValue("@ProjectId", projectId_DG);
                    cmd_Industry_advisor.Parameters.AddWithValue("@AdvisorRole", 14);
                    cmd_Industry_advisor.Parameters.AddWithValue("@AssignmentDate", assignmentDate_picker.Value);
                    con.Open();
                    cmd_Industry_advisor.ExecuteNonQuery();
                    con.Close();

                    // MessageBox.Show(getAdvisorId(industryAdvisor_combox.SelectedItem.ToString()).ToString());


                    MessageBox.Show("Data editted sucessfully!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    form_Clear();
                    bind_Projects_DG();
                }
                else
                {
                    MessageBox.Show("Fill all feilds correctly or Two advisors cannot be same.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }

        private void search_btn_Click_1(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile searching data";
            try
            {
                string cmd = "Select * From Project where Title like '%'+@Title+'%' ";
                SqlDataAdapter sda = new SqlDataAdapter(cmd, con);
                sda.SelectCommand.Parameters.AddWithValue("@Title", search_box_txt.Text.Trim());
                DataTable data = new DataTable();
                sda.Fill(data);
                dataGridView_Projects.DataSource = data;
                dataGridView_Projects.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Data Validations

        private bool Is_ValidData()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(title_txt.Text)) { isValid= false; }
            if (string.IsNullOrEmpty(description_txt.Text)) { isValid = false; }

            if (mainAdvisor_combobox.SelectedIndex==-1) { isValid = false; }
            if (co_advisor_combobox.SelectedIndex == -1) { isValid = false; }
            if (industryAdvisor_combox.SelectedIndex == -1) { isValid = false; }

            return isValid;
        }

        private void title_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(title_txt.Text))
            {
                title_txt.Focus();
                eProvider_title.SetError(title_txt, "Enter project title");
            }
            else
            {
                eProvider_title.Clear();
            }
        }

        private void description_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(title_txt.Text))
            {
                description_txt.Focus();
                eProvider_description.SetError(title_txt, "Enter  description of project");
            }
            else
            {
                eProvider_title.Clear();
            }
        }



        #endregion

    }
}
