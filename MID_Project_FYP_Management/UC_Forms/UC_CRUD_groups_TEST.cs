﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using static System.ComponentModel.Design.ObjectSelectorEditor;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_CRUD_groups_TEST : UserControl
    {
        #region Variables
        static readonly string cs = ConfigurationManager.ConnectionStrings["dbProjectA"].ConnectionString;
        readonly SqlConnection con = new SqlConnection(cs);
        #endregion
        public UC_CRUD_groups_TEST()
        {
            InitializeComponent();
            bindStudentData_DG(true);
            bindGroupData_DG();
            dataGridView_GroupDetails.ColumnHeadersHeight = 20;
            dataGridView_Unselected_Students.ColumnHeadersHeight = 20;
            dataGridView_Groups.ColumnHeadersHeight = 20;
        }
        #region DataGrid View functions
        private void bindGroupData_DG()
        {
            string query = "SELECT Id, Created_On FROM [ProjectA].[dbo].[Group]";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView_Groups.DataSource = dt;
            dataGridView_Groups.Refresh();
        }
        private void bindStudentData_DG(bool bindCheckbox)
        {
            string query = "SELECT S.Id,S.RegistrationNo, CONCAT(P.FirstName,' ',P.LastName)"+
            " FROM Student S" +
            " JOIN Person P" +
            " ON P.Id = S.Id"+
            " JOIN Lookup L" +
            " ON L.Id = P.Gender" +
            " WHERE S.Id NOT IN(SELECT GS.StudentId FROM GroupStudent GS JOIN Lookup L1 ON L1.Id = GS.Status WHERE Value = 'Active')";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView_Unselected_Students.DataSource = dt;
            dataGridView_Unselected_Students.Refresh();

            if (bindCheckbox)
            {

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                DataGridViewCheckBoxColumn checkBox_col = dataGridViewCheckBoxColumn;
                checkBox_col.Width = 20;
                checkBox_col.Name = "stuCheck";
                checkBox_col.HeaderText = "";
                dataGridView_Unselected_Students.Columns.Insert(0, checkBox_col);
            }
        }
        private void dataGridView_Unselected_Students_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView_Unselected_Students.SelectedRows[0].Cells["stuCheck"].Value = !Convert.ToBoolean((dataGridView_Unselected_Students.SelectedRows[0].Cells["stuCheck"].Value));

        }
        private void dataGridView_Groups_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bindGroupDetails_DG((int)(dataGridView_Groups.SelectedRows[0].Cells["Id"].Value), true);

        }

        private void bindGroupDetails_DG(int gId, bool add_Btns)
        {
            string query = $"SELECT GS.GroupId,GS.StudentId,CONCAT(P.FirstName,' ',P.LastName) Name ,S.RegistrationNo,L1.Value 'Status' " +
                $" FROM GroupStudent GS" +
                $" JOIN Lookup L1 ON L1.Id=GS.Status " +
                $" JOIN Person P ON GS.StudentId=P.Id" +
                $" JOIN Student S ON S.Id=GS.StudentId" +
                $" WHERE GroupId={gId}";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView_GroupDetails.DataSource = dt;
            dataGridView_GroupDetails.Refresh();

            if (add_Btns && dataGridView_GroupDetails.ColumnCount == 5)
            {
                DataGridViewButtonColumn col_Button_Active = new DataGridViewButtonColumn();

                //col_Button_Active.Width = 20;
                col_Button_Active.Name = "stuActive_btn";
                col_Button_Active.Text = "Active";

                col_Button_Active.HeaderText = "";
                col_Button_Active.UseColumnTextForButtonValue = true;
                dataGridView_GroupDetails.Columns.Insert(5, col_Button_Active);

                DataGridViewButtonColumn col_Button_InActive = new DataGridViewButtonColumn();

                //col_Button_Active.Width = 20;
                col_Button_InActive.Name = "stuInActive_btn";
                col_Button_InActive.Text = "In Active";

                col_Button_InActive.HeaderText = "";
                col_Button_InActive.UseColumnTextForButtonValue = true;
                dataGridView_GroupDetails.Columns.Insert(6, col_Button_InActive);
            }

        }

        private void dataGridView_GroupDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //dataGridView_GroupDetails.SelectedRows[0].Cells["stuCheckGroup"].Value = !Convert.ToBoolean((dataGridView_GroupDetails.SelectedRows[0].Cells["stuCheckGroup"].Value));
            //Select students and use for add and delete member
        }
        private void dataGridView_GroupDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView_GroupDetails.Columns["stuInActive_btn"].Index && e.RowIndex >= 0)
            {
                //MessageBox.Show("In active Button clicked");
                //MessageBox.Show(e.RowIndex.ToString(),"Row index");
                string previousStatus = getData_From_Group_Detail(e.RowIndex, "Status");
                MessageBox.Show(previousStatus);
                if (previousStatus != "InActive")
                {
                    //wRITE code to in active
                    int groupId = int.Parse(getData_From_Group_Detail(e.RowIndex, "GroupId"));
                    int studentId = int.Parse(getData_From_Group_Detail(e.RowIndex, "StudentId"));

                    changeStatus(groupId, studentId, "InActive");
                }
                else
                {
                    MessageBox.Show("Group member is already in active", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else if (e.ColumnIndex == dataGridView_GroupDetails.Columns["stuActive_btn"].Index && e.RowIndex >= 0)
            {
                string previousStatus = getData_From_Group_Detail(e.RowIndex, "Status");
                MessageBox.Show(previousStatus);
                if (previousStatus != "Active")
                {
                    int studentId = int.Parse(getData_From_Group_Detail(e.RowIndex, "StudentId"));
                    int groupId = int.Parse(getData_From_Group_Detail(e.RowIndex, "GroupId"));
                    if (!checkOtherGroups(studentId, groupId))
                    {
                        //wRITE code to in active
                        //MessageBox.Show(memberCount.ToString(), "Group Id");
                        if (getNumber_ActiveMembers(groupId) < 6)
                        {
                            changeStatus(groupId, studentId, "Active");
                        }
                        else
                        {
                            MessageBox.Show("Active Group members should be alleast 3 and maximum 5", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Student is present in other group so, cannot add in two groups at a time", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Group member is already active", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }

        #endregion

        #region Button Clicks
        private void search_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile searching data";
            try
            {
                string query = "SELECT S.Id,S.RegistrationNo, CONCAT(P.FirstName,' ',P.LastName)" +
                " FROM Student S" +
                " JOIN Person P" +
                " ON P.Id = S.Id" +
                " JOIN Lookup L" +
                " ON L.Id = P.Gender" +
                " WHERE S.Id NOT IN(SELECT GS.StudentId FROM GroupStudent GS JOIN Lookup L1 ON L1.Id = GS.Status WHERE Value = 'Active' )" +
                " AND P.FirstName like '%'+@FirstName+'%'";
                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                sda.SelectCommand.Parameters.AddWithValue("@FirstName", search_box_txt.Text.Trim());
                DataTable data = new DataTable();
                sda.Fill(data);
                dataGridView_Unselected_Students.DataSource = data;
                dataGridView_Unselected_Students.Refresh();
                if (dataGridView_Unselected_Students.RowCount == 0)
                {
                    bindStudentData_DG(false);
                    MessageBox.Show("Result Not found!", "Search Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    search_box_txt.Text = "";
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Add_Member_btn_Click(object sender, EventArgs e)
        {
            int memberCount = 0;
            foreach (DataGridViewRow row in dataGridView_Unselected_Students.Rows)
            {
                bool value = Convert.ToBoolean(row.Cells["stuCheck"].Value);
                MessageBox.Show(value.ToString(), "Check box value");

                if (value)
                {
                    memberCount++;
                }
            }
            memberCount += getNumber_ActiveMembers((int)dataGridView_Groups.SelectedRows[0].Cells[0].Value);
            // MessageBox.Show(memberCount.ToString(), "Active Group Members");
            if (memberCount < 6) //Can also check further conditions before creating group.. 
            {
                // Create a group on basis of  system time and date
                string errorMessege = "\nWhile adding Group members";
                try
                {

                    int groupId = (int)(dataGridView_Groups.SelectedRows[0].Cells[0].Value);
                    // MessageBox.Show(groupId.ToString(), "Group Id");

                    foreach (DataGridViewRow row in dataGridView_Unselected_Students.Rows)
                    {
                        bool value = Convert.ToBoolean(row.Cells["stuCheck"].Value);
                        if (value)
                        {
                            string query = "INSERT INTO GroupStudent values(@GroupId,@StudentId,@Status,@AssignmentDate)";
                            SqlCommand cmd_groupStudent = new SqlCommand(query, con);
                            cmd_groupStudent.Parameters.AddWithValue("@GroupId", groupId);
                            cmd_groupStudent.Parameters.AddWithValue("@StudentId", row.Cells["Id"].Value);
                            cmd_groupStudent.Parameters.AddWithValue("@Status", 3);
                            cmd_groupStudent.Parameters.AddWithValue("@AssignmentDate", DateTime.Now.ToString());

                            con.Open();
                            cmd_groupStudent.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    bindGroupDetails_DG(groupId, false);
                    bindStudentData_DG(true);
                    MessageBox.Show("Group members added", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception err)
                {
                    MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    con.Close();
                }
            }
            else
            {
                MessageBox.Show("Group members should be alleast 3 and maximum 5", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }
        private void makeGroup_btn_Click(object sender, EventArgs e)
        {
            int memberCount = 0;
            foreach (DataGridViewRow row in dataGridView_Unselected_Students.Rows)
            {
                bool value = Convert.ToBoolean(row.Cells["stuCheck"].Value);
                MessageBox.Show(value.ToString(), "Check box value");

                if (value) 
                {
                    memberCount++;
                }
            }
            MessageBox.Show(memberCount.ToString(), "Group Members");
            if (memberCount>=3 && memberCount < 6) //Can also check further conditions before creating group.. 
            {
                // Create a group on basis of  system time and date
                string errorMessege = "\nWhile making Group";
                try
                {
                    SqlCommand cmd_group = new SqlCommand("INSERT INTO [ProjectA].[dbo].[Group] values(@Created_On)", con);
                    cmd_group.Parameters.AddWithValue("@Created_On", DateTime.Now.ToString());

                    con.Open();
                    cmd_group.ExecuteNonQuery();
                    con.Close();

                    int groupId = getGroupId(DateTime.Now.ToString());
                    MessageBox.Show(groupId.ToString(), "Group Id");

                    foreach (DataGridViewRow row in dataGridView_Unselected_Students.Rows)
                    {
                        bool value = Convert.ToBoolean(row.Cells["stuCheck"].Value);
                        if (value)
                        {
                            string query = "INSERT INTO GroupStudent values(@GroupId,@StudentId,@Status,@AssignmentDate)";
                            SqlCommand cmd_groupStudent = new SqlCommand(query, con);
                            cmd_groupStudent.Parameters.AddWithValue("@GroupId", groupId);
                            cmd_groupStudent.Parameters.AddWithValue("@StudentId", row.Cells["Id"].Value);
                            cmd_groupStudent.Parameters.AddWithValue("@Status", 3);
                            cmd_groupStudent.Parameters.AddWithValue("@AssignmentDate", DateTime.Now.ToString());

                            con.Open();
                            cmd_groupStudent.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    bindGroupData_DG();
                    MessageBox.Show("Group made", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception err)
                {
                    MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    con.Close();
                }
            }
            else 
            {
                    MessageBox.Show("Group members should be alleast 3 and maximum 5", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        #endregion

        #region Database Attribute getting functions

        private int getGroupId(string date)
        {

            string errorMessege = "\nWhile 'getting Group Id'";
            int id = -1; //For error purposes

            string query = $" select id " +
                $" from [ProjectA].[dbo].[Group]" +
                $" where " +
                $" Created_On = '{date}' ";
               

            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }

            return id;
        }

        private int getNumber_ActiveMembers(int groupId)
        {
            int count = -1;
            string errorMessege = "\nWhile checking active members in function";
            try
            {
                string query = $" SELECT COUNT(*) FROM GroupStudent GS JOIN Lookup L ON L.Id=GS.Status WHERE GroupId= '{groupId}' AND L.Value= 'Active' ";
                SqlCommand cmd = new SqlCommand(query, con);

                con.Open();
                count = (int)cmd.ExecuteScalar();
                con.Close();
                //MessageBox.Show(count.ToString(), "Member count in function");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return count;
        }
     

        private bool checkOtherGroups(int studentId, object groupId)
        {
            bool isPresent = false;
            string errorMessege = "\nWhile checking student presence in other groups";
            try 
            {
                int g_Id = -1;
                string query = $"SELECT GS.GroupId  " +
                    $" FROM GroupStudent GS " +
                    $" JOIN Lookup L ON L.Id=GS.Status" +
                    $" WHERE GS.GroupId<>{groupId} AND GS.StudentId={studentId} AND L.Value='Active' ";
                SqlCommand cmd = new SqlCommand(query, con);
                
                con.Open();
                var f = cmd.ExecuteScalar();
                if (f != null)
                {
                    if (int.TryParse(f.ToString(),out g_Id))
                    {
                        return true;
                    }

                }
                con.Close();

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return isPresent;
        }
        private string checkStatus_DB(int groupId, object studentId)
        {
            string status = "";
            string errorMessege = "\nWhile checking status in function";
            try
            {
                string query = $" SELECT L.Value Status FROM GroupStudent GS JOIN Lookup L ON L.Id=GS.Status WHERE GroupId= '{groupId}' AND StudentId= '{studentId}' ";
                SqlCommand cmd = new SqlCommand(query, con);

                con.Open();
                status = (string)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return status;
        }

        private int getId_Lookup(string value, string category)
        {
            string errorMessege = $"\nWhile 'getting {value} Id From Lookup Table'";
            int id = -1; //For error purposes

            string query = $" select id " +
                $" from [ProjectA].[dbo].[Lookup]" +
                $" where " +
                $" Category= '{category}' AND Value= '{value}'";


            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return id;
        }


        private string getData_From_Group_Detail(int rowIndex, string columnName)
        {
            return dataGridView_GroupDetails.Rows[rowIndex].Cells[columnName].Value.ToString();
        }

        #endregion

        #region Helper Functions
        private void changeStatus( int GroupId, object studentId, string statusRequired)
        {
            string errorMessege = "\nWhile changing status in function";
            try
            {
                int statusId = getId_Lookup(statusRequired, "STATUS");
                //MessageBox.Show(statusId.ToString());

                string query = " UPDATE GroupStudent SET Status=@Status" +
                               " WHERE GroupId=@GroupId AND StudentId=@StudentId";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@Status", statusId);

                cmd.Parameters.AddWithValue("@GroupId", GroupId);
                cmd.Parameters.AddWithValue("@StudentId", studentId);
                

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                string currentStatus=checkStatus_DB(GroupId, studentId);
                if (currentStatus ==statusRequired) 
                {
                    MessageBox.Show($"Status changed to {statusRequired}", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    bindGroupDetails_DG(GroupId,false);
                    bindStudentData_DG(false);
                }
                else 
                {
                    MessageBox.Show($"Status not changed to {statusRequired}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        #endregion

    }
}
