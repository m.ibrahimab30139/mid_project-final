﻿using MID_Project_FYP_Management.Helper_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_Evaluations_CRUD : UserControl
    {
        public UC_Evaluations_CRUD()
        {
            InitializeComponent();
        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void evaluateGroup_btn_Click(object sender, EventArgs e)
        {
            var uc = new UC_SelectGroupFor_Evaluation();
            UserControl_Helper.addUserControl(uc, panelForm_Parent);
        }

        private void addEvaluation_btn_Click(object sender, EventArgs e)
        {
            var uc = new UC_AddEvaluation();
            UserControl_Helper.addUserControl(uc, panelForm_Parent);
        }
    }
}
