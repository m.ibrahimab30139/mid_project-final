﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.ComponentModel.Design.ObjectSelectorEditor;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_AssignProject : UserControl
    {
        #region Variables 
        static readonly string cs = ConfigurationManager.ConnectionStrings["dbProjectA"].ConnectionString;
        readonly SqlConnection connection= new SqlConnection(cs);
        #endregion
        public UC_AssignProject()
        {
            InitializeComponent();
            disable_Project_Controls();
            fillProject_Combobox();

            bind_UnAssignedGroups(false);
            bind_AssignedGroups();
            dataGridView_Projectdetails.ColumnHeadersHeight = 20;
            dataGridView_UnasignedG_Details.ColumnHeadersHeight = 20;
        }

        #region Form Controls
        private void disable_Project_Controls() 
        {
            description_txt.Enabled = false;
            mainAdvisor_txt.Enabled = false;
            co_Advisor_txt.Enabled = false;
            industryAdvisor_txt.Enabled = false;
        }

        #endregion

        #region DataGrid View functions
        private void bind_AssignedGroups() 
        {
            string errorMessege = "\nWhile binding ASSIGNED groups";
            try
            {
                string query = "    SELECT *" +
                                "   FROM[ProjectA].[dbo].[GROUP] G" +
                                "   WHERE G.Id IN" +
                                "   (SELECT GP.GroupId" +
                                "   FROM GroupProject GP)";
                SqlCommand cmd = new SqlCommand(query, connection);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_AssignedGroups.DataSource = dt;
                dataGridView_AssignedGroups.Refresh();

             
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bind_UnAssignedGroups(bool bindCheckbox) 
        {
            string errorMessege = "\nWhile binding unasigned groups";
            try
            {
                string query = "    SELECT *" +
                                "   FROM[ProjectA].[dbo].[GROUP] G"+
                                "   WHERE G.Id NOT IN"+
                                "   (SELECT GP.GroupId"+
                                "   FROM GroupProject GP)";
                SqlCommand cmd = new SqlCommand(query, connection);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_UnasignedGroups.DataSource = dt;
                dataGridView_UnasignedGroups.Refresh();

                if (bindCheckbox)
                {

                    DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                    DataGridViewCheckBoxColumn checkBox_col = dataGridViewCheckBoxColumn;
                    checkBox_col.Width = 20;
                    checkBox_col.Name = "groupCheck";
                    checkBox_col.HeaderText = "";
                    dataGridView_UnasignedGroups.Columns.Insert(0, checkBox_col);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dataGridView_UnasignedGroups_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bindGroupDetails_DG((int)dataGridView_UnasignedGroups.SelectedRows[0].Cells["Id"].Value);
            //dataGridView_UnasignedGroups.SelectedRows[0].Cells["groupCheck"].Value = !Convert.ToBoolean((dataGridView_UnasignedGroups.SelectedRows[0].Cells["groupCheck"].Value));
        }
        private void bindGroupDetails_DG(int groupId)
        {
            string errorMessege = "\nWhile binding unasigned groups details";
            try
            {
                string query = $"SELECT GS.GroupId,GS.StudentId,CONCAT(P.FirstName,' ',P.LastName) Name ,S.RegistrationNo,L1.Value 'Status' " +
                $" FROM GroupStudent GS" +
                $" JOIN Lookup L1 ON L1.Id=GS.Status " +
                $" JOIN Person P ON GS.StudentId=P.Id" +
                $" JOIN Student S ON S.Id=GS.StudentId" +
                $" WHERE GroupId={groupId} AND L1.Value= 'Active' ";
                SqlCommand cmd = new SqlCommand(query, connection);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_UnasignedG_Details.DataSource = dt;
                dataGridView_UnasignedG_Details.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGridView_AssignedGroups_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string errorMessege = "\nWhile binding project details data";
            try
            {
                string query = "SELECT P.Title,P.Description " +
                    " FROM GroupProject GP " +
                    " JOIN Project P ON P.Id=GP.ProjectId";
                SqlCommand cmd = new SqlCommand(query, connection);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Projectdetails.DataSource = dt;
                dataGridView_Projectdetails.Refresh();

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Data filling functions
        private void projectTitle_Combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectTitle = projectTitle_Combobox.SelectedItem.ToString();
            //MessageBox.Show(projectTitle);
            string mainAdvisor = get_Project_Advisors(projectTitle, "Main Advisor");
            string co_Advisor = get_Project_Advisors(projectTitle, "Co-Advisror");
            string industryAdvisor = get_Project_Advisors(projectTitle, "Industry Advisor");

            mainAdvisor_txt.Text = mainAdvisor;
            co_Advisor_txt.Text = co_Advisor;
            industryAdvisor_txt.Text = industryAdvisor;

            description_txt.Text = getProject_Details(projectTitle, "Description");

            //MessageBox.Show(mainAdvisor, "Main Advisor");
            //MessageBox.Show(co_Advisor, "Co Advisor");
            //MessageBox.Show(industryAdvisor, "Industry Advisor");
        }
        private void fillProject_Combobox() 
        {
            string errorMessege = "\nWhile binding project title combobox";
            try
            {
                string query =
                   " SELECT P.Title" +
                   " FROM Project P" +
                   " WHERE P.Id NOT IN" +
                   " (SELECT GP.ProjectId FROM GroupProject GP)";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    string item = dataReader.GetString(0); //Because value is at 1 index
                    projectTitle_Combobox.Items.Add(item);
                }
                connection.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connection.Close();
            }
        }

        #endregion

        #region Database Attribute getting functions

        private string get_Project_Advisors(string projectTitle, string advisorRole)
        {
            string errorMessege = $"\nWhile geting {advisorRole} name";
            string Name = "";
            string project_Id = getProject_Details(projectTitle,"Id");
            int AdvisorRole_Id = getId_Lookup(advisorRole, "ADVISOR_ROLE");
            try 
            {
                string query = $"SELECT CONCAT(P.FirstName,' ',P.LastName)" +
                             $" FROM ProjectAdvisor PA" +
                             $" JOIN Person P ON PA.AdvisorId=P.Id" +
                             $" WHERE PA.ProjectId= {project_Id} AND PA.AdvisorRole= {AdvisorRole_Id} ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);

                connection.Open();
                Name = (string)sqlCommand.ExecuteScalar();
                connection.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connection.Close();
            }
            return Name;
        }

        private string getProject_Details(string projectTitle, string Attribute)
        {
            string errorMessege = $"\nWhile getting project {Attribute} ";
            string outPut = "";
            try
            {
                string query =
                   $" SELECT P.{Attribute}" +
                   $" FROM Project P" +
                   $" WHERE P.Title = '{projectTitle}'";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
               
                connection.Open();
                outPut = sqlCommand.ExecuteScalar().ToString();
                connection.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connection.Close();
            }
            MessageBox.Show(outPut,$"Project Tabel{Attribute}");
            return outPut;
        }

        private int getId_Lookup(string value, string category)
        {
            string errorMessege = $"\nWhile 'getting {value} Id From Lookup Table'";
            int id = -1; //For error purposes

            string query = $" select id " +
                $" from [ProjectA].[dbo].[Lookup]" +
                $" where " +
                $" Category= '{category}' AND Value= '{value}'";


            SqlCommand cmd = new SqlCommand(query, connection);
            try
            {
                MessageBox.Show(connection.State.ToString(), "Connection State");
                connection.Open();
                id = (int)cmd.ExecuteScalar();
                connection.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connection.Close();
            }

            return id;
        }

        #endregion

        #region Button Clicks
        private void AssignProject_btn_Click(object sender, EventArgs e)
        {
            //First check the active members if greater than 1 then assign prject else show the warning.
            if (dataGridView_UnasignedG_Details.RowCount > 0) 
            {
                string errorMessege = "\nWhile assigning project to Group";
                try 
                {
                    string projectTitle = projectTitle_Combobox.SelectedItem.ToString();
                    string project_Id = getProject_Details(projectTitle, "Id");
                    int groupId = (int)dataGridView_UnasignedGroups.SelectedRows[0].Cells["Id"].Value;

                    string query = $"INSERT INTO GroupProject values(@ProjectId,@GroupId,@AssignmentDate)";
                    SqlCommand cmd_groupProject = new SqlCommand(query, connection);
                    cmd_groupProject.Parameters.AddWithValue("@ProjectId", project_Id);
                    cmd_groupProject.Parameters.AddWithValue("@GroupId", groupId);
                    cmd_groupProject.Parameters.AddWithValue("@AssignmentDate", DateTime.Now.ToString());

                    connection.Open();
                    cmd_groupProject.ExecuteNonQuery();
                    connection.Close();

                    bind_AssignedGroups();
                    bind_UnAssignedGroups(false);
                    dataGridView_UnasignedG_Details.Rows.Clear();

                }
                catch (Exception err)
                {
                    MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    connection.Close();
                }
            }
            else 
            {
                MessageBox.Show("No active members in this Group.So, project cannot be assigned", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

    }
}
