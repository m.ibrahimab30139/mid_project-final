﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_CRUD_Advisor : UserControl
    {
        public UC_CRUD_Advisor()
        {
            InitializeComponent();

            bindData_DG();
            fill_GenderCombox();
            fill_DesignationCombox();

            designation_Combobox.SelectedIndex = 0;
            gender_combox.SelectedIndex = 0;
            dataGridView_Advisor.ColumnHeadersHeight=20;
        }
        #region variables
        static readonly string cs = ConfigurationManager.ConnectionStrings["dbProjectA"].ConnectionString;
        readonly SqlConnection con = new SqlConnection(cs);
        readonly string emailPattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

        string personID_DG = "";

        #endregion

        #region DataGrid View functions

        private void bindData_DG()
        {
            string errorMessege = "\nWhile binding advisors datagrid view";
            try
            {
                //SqlCommand cmd = new SqlCommand("Select * from Student", con);
                SqlCommand cmd = new SqlCommand(
                    "Select a.id,L2.value,a.salary,p.FirstName, p.LastName,p.Email,L1.value,p.DateOfBirth,p.Contact " + " from Advisor a " + " join Person p " + " on p.id=a.id join Lookup L1 on L1.Id=P.Gender join Lookup L2 on L2.Id=a.Designation", con); //join multiple table person and student

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView_Advisor.DataSource = dt;
                dataGridView_Advisor.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }

        }
        private void dataGridView_Advisor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            personID_DG = dataGridView_Advisor.SelectedRows[0].Cells[0].Value.ToString();
            designation_Combobox.Text = dataGridView_Advisor.SelectedRows[0].Cells[1].Value.ToString();
            salary_txt.Text = dataGridView_Advisor.SelectedRows[0].Cells[2].Value.ToString();


            F_Name_txt.Text = dataGridView_Advisor.SelectedRows[0].Cells[3].Value.ToString();
            L_Name_txt.Text = dataGridView_Advisor.SelectedRows[0].Cells[4].Value.ToString();

            email_txt.Text = dataGridView_Advisor.SelectedRows[0].Cells[5].Value.ToString();
            gender_combox.Text = dataGridView_Advisor.SelectedRows[0].Cells[6].Value.ToString();

            dob_picker.Text = dataGridView_Advisor.SelectedRows[0].Cells[7].Value.ToString();
            contact_txt.Text = dataGridView_Advisor.SelectedRows[0].Cells[8].Value.ToString();


        }

        #endregion

        #region Data Filling Functions

        private void fill_GenderCombox()
        {
            string query = "select * from Lookup where category='Gender'";
            SqlCommand sqlCommand = new SqlCommand(query, con);
            con.Open();
            SqlDataReader dataReader = sqlCommand.ExecuteReader();
            while (dataReader.Read())
            {
                string gender = dataReader.GetString(1); //Because value is at 1 index
                gender_combox.Items.Add(gender);
            }
            con.Close();
        }
        private void fill_DesignationCombox()
        {
            string query = "select * from Lookup where category='Designation'";
            SqlCommand sqlCommand = new SqlCommand(query, con);
            con.Open();
            SqlDataReader dataReader = sqlCommand.ExecuteReader();
            while (dataReader.Read())
            {
                string item = dataReader.GetString(1); //Because value is at 1 index
                designation_Combobox.Items.Add(item);
            }
            con.Close();
        }



        #endregion

        #region Format functions

        private void form_Clear()
        {
            contact_txt.Text = "";
            email_txt.Text = "";
            salary_txt.Text = "";
            F_Name_txt.Text = "";
            L_Name_txt.Text = "";

        }

        #endregion

        #region Database attribute getting Functions

        private int getIdFormPersonTable(int gender)
        {
            string errorMessege = "\nWhile 'getting Person Id' Student";
            int id = -1; //For error purposes

            string query = $" select id " +
                $" from Person " +
                $" where " +
                $" FirstName = '{F_Name_txt.Text}' and " +
                $" LastName = '{L_Name_txt.Text}' and " +
                $" contact = '{contact_txt.Text}' and " +
                $" email = '{email_txt.Text}' and " +
                $" Gender = '{gender}'";

            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                id = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return id;
        }

        #endregion

        #region Data Validation

        private bool Is_ValidData()
        {
            bool is_Valid = true;

            if (string.IsNullOrEmpty(contact_txt.Text)) { is_Valid = false; }
            if (string.IsNullOrEmpty(F_Name_txt.Text)) { is_Valid = false; }
            if (string.IsNullOrEmpty(L_Name_txt.Text)) { is_Valid = false; }
            if (string.IsNullOrEmpty(salary_txt.Text)) { is_Valid = false; }
            if (string.IsNullOrEmpty(email_txt.Text)) { is_Valid = false; }

            return is_Valid;
        }
        private void F_Name_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void F_Name_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(F_Name_txt.Text))
            {
                F_Name_txt.Focus();
                eProvider_F_name.SetError(F_Name_txt, "Enter a first name");
                // disable_Add_btn();
            }
            else
            {
                eProvider_F_name.Clear();
                //enable_Add_Edit_btn();
            }
        }

        private void L_Name_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(L_Name_txt.Text))
            {
                L_Name_txt.Focus();
                eProvider_L_name.SetError(L_Name_txt, "Enter a last name");
                // disable_Add_btn();
            }
            else
            {
                eProvider_L_name.Clear();
                //enable_Add_Edit_btn();
            }
        }

        private void L_Name_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void contact_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void contact_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(contact_txt.Text))
            {
                contact_txt.Focus();
                eProvider_contact.SetError(contact_txt, "Enter contact number");
                // disable_Add_btn();
            }
            else
            {
                eProvider_contact.Clear();
                //enable_Add_Edit_btn();
            }
        }
        private void email_txt_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(email_txt.Text, emailPattern) || string.IsNullOrEmpty(email_txt.Text))
            {
                email_txt.Focus();
                eProvider__email.SetError(email_txt, "Enter a valid email");

            }
            else
            {
                eProvider__email.Clear();
            }
        }

        private void salary_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void salary_txt_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(salary_txt.Text))
            {
                salary_txt.Focus();
                eProvider_salary.SetError(salary_txt, "Enter a first name");
                // disable_Add_btn();
            }
            else
            {
                eProvider_salary.Clear();
                //enable_Add_Edit_btn();
            }
        }

        #endregion

        #region Button Clicks

        private void addAdvisor_btn_Click_1(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile 'Adding Advisor'";

            try
            {
                //MessageBox.Show(Is_ValidData().ToString());
                if (Is_ValidData())
                {
                    SqlCommand cmd_person = new SqlCommand("INSERT INTO Person values(@FirstName,@LastName, @Contact,@Email,@DateOfBirth,@Gender)", con);
                    cmd_person.Parameters.AddWithValue("@FirstName", F_Name_txt.Text);
                    cmd_person.Parameters.AddWithValue("@LastName", L_Name_txt.Text);
                    cmd_person.Parameters.AddWithValue("@Contact", contact_txt.Text);
                    cmd_person.Parameters.AddWithValue("@Email", email_txt.Text);
                    cmd_person.Parameters.AddWithValue("@DateOfBirth", dob_picker.Value);
                    if (gender_combox.SelectedIndex == 0)
                    {
                        cmd_person.Parameters.AddWithValue("@Gender", 1); //Male
                    }
                    else if (gender_combox.SelectedIndex == 1)
                    {
                        cmd_person.Parameters.AddWithValue("@Gender", 2); //Female

                    }

                    con.Open();
                    cmd_person.ExecuteNonQuery();
                    con.Close();


                    //Getting related person Id from db
                    int personId = getIdFormPersonTable(gender_combox.SelectedIndex + 1);

                    // DB Data insertion of Student table.
                    SqlCommand cmd_advisor = new SqlCommand("INSERT INTO Advisor values(@Id,@Designation,@Salary)", con);
                    cmd_advisor.Parameters.AddWithValue("@Id", personId);
                    if (designation_Combobox.SelectedIndex == 0)
                    {
                        cmd_advisor.Parameters.AddWithValue("@Designation", 6); //Professor
                    }
                    else if (designation_Combobox.SelectedIndex == 1)
                    {
                        cmd_advisor.Parameters.AddWithValue("@Designation", 7); //Asso.Professor

                    }
                    else if (designation_Combobox.SelectedIndex == 2)
                    {
                        cmd_advisor.Parameters.AddWithValue("@Designation", 8); //Assit.Professor

                    }
                    else if (designation_Combobox.SelectedIndex == 3)
                    {
                        cmd_advisor.Parameters.AddWithValue("@Designation", 9); //Lecture

                    }
                    else if (designation_Combobox.SelectedIndex == 4)
                    {
                        cmd_advisor.Parameters.AddWithValue("@Designation", 10); //Industry professional
                    }

                    cmd_advisor.Parameters.AddWithValue("@Salary", salary_txt.Text.ToString());

                    MessageBox.Show(designation_Combobox.SelectedIndex.ToString());


                    con.Open();
                    cmd_advisor.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data added sucessfully!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    form_Clear();
                    bindData_DG();
                }
                else
                {
                    MessageBox.Show("Fill all feilds", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    con.Close();

                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }
        private string getLookup_Details(string category, string value, string requiredAttribute)
        {
            string id = "";
            string errorMessege = $"\nWhile geting {category} from lookupTable";
            try
            {
                string query = $"SELECT {requiredAttribute} FROM Lookup WHERE Lookup.Value='{value}'";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                id = cmd.ExecuteScalar().ToString();
                con.Close();
                return id;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
            return id;
        }
        private void editAdvisor_btn_Click_1(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile 'Editting Advisor'";

            try
            {
                //MessageBox.Show(Is_ValidData().ToString());
                if (Is_ValidData())
                {
                    SqlCommand cmd_person = new SqlCommand("UPDATE Person SET FirstName=@FirstName,LastName=@LastName, Contact=@Contact,Email=@Email,DateOfBirth=@DateOfBirth,Gender=@Gender WHERE Id=@Id", con);
                    cmd_person.Parameters.AddWithValue("@FirstName", F_Name_txt.Text);
                    cmd_person.Parameters.AddWithValue("@LastName", L_Name_txt.Text);
                    cmd_person.Parameters.AddWithValue("@Contact", contact_txt.Text);
                    cmd_person.Parameters.AddWithValue("@Email", email_txt.Text);
                    cmd_person.Parameters.AddWithValue("@DateOfBirth", dob_picker.Value);
                    cmd_person.Parameters.AddWithValue("@Id", personID_DG);
                    cmd_person.Parameters.AddWithValue("@Gender", getLookup_Details("Gender", gender_combox.SelectedItem.ToString(), "Id"));

                    con.Open();
                    cmd_person.ExecuteNonQuery();
                    con.Close();


                    SqlCommand cmd_advisor = new SqlCommand("UPDATE Advisor SET Designation=@Designation,Salary=@Salary WHERE Id=@Id", con);
                    cmd_advisor.Parameters.AddWithValue("@Id", personID_DG);
                    cmd_advisor.Parameters.AddWithValue("@Designation", getLookup_Details("DESIGNATION",designation_Combobox.SelectedItem.ToString(),"Id")); //Professor

                    cmd_advisor.Parameters.AddWithValue("@Salary", salary_txt.Text.ToString());

                    MessageBox.Show(designation_Combobox.SelectedIndex.ToString());


                    con.Open();
                    cmd_advisor.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data editted sucessfully!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    form_Clear();
                    bindData_DG();
                }
                else
                {
                    MessageBox.Show("Fill all feilds", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }

        private void search_btn_Click(object sender, EventArgs e)
        {
            string errorMessege = "\nWhile searching data";
            try
            {
                string cmd = "Select a.id,L2.value,a.salary,p.FirstName, p.LastName,p.Email,L1.value,p.DateOfBirth,p.Contact from Advisor a join Person p on p.id=a.id join Lookup L1 on L1.Id=P.Gender join Lookup L2 on L2.Id=a.Designation where FirstName like '%'+@FirstName+'%' ";
                SqlDataAdapter sda = new SqlDataAdapter(cmd, con);
                sda.SelectCommand.Parameters.AddWithValue("@FirstName", search_box_txt.Text.Trim());
                DataTable data = new DataTable();
                sda.Fill(data);
                dataGridView_Advisor.DataSource = data;
                dataGridView_Advisor.Refresh();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString() + errorMessege, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
            }
        }




        #endregion

        private void gender_combox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       
    }
}
