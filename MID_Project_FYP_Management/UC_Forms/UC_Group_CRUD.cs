﻿using MID_Project_FYP_Management.Helper_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_Group_CRUD : UserControl
    {
        public UC_Group_CRUD()
        {
            InitializeComponent();
        }

        private void addGroup_btn_Click(object sender, EventArgs e)
        {
            var uc =new UC_Group_Add();
            UserControl_Helper.addUserControl(uc, panelForm_Parent);
        }

        private void assignProject_btn_Click(object sender, EventArgs e)
        {
            var uc = new UC_AssignProject();
            UserControl_Helper.addUserControl(uc, panelForm_Parent);
        }

        private void UpdateGroup_btn_Click(object sender, EventArgs e)
        {
            var uc = new UC_updateGroup();
            UserControl_Helper.addUserControl(uc, panelForm_Parent);
        }
    }
}
