﻿namespace MID_Project_FYP_Management.UC_Forms
{
    partial class UC_AssignProject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutParentPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutTitlePanel = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.industryAdvisor_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.description_txt = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.projectTitle_Combobox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.mainAdvisor_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.co_Advisor_txt = new Guna.UI2.WinForms.Guna2TextBox();
            this.AssignProject_btn = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView_Projectdetails = new Guna.UI2.WinForms.Guna2DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView_AssignedGroups = new Guna.UI2.WinForms.Guna2DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView_UnasignedG_Details = new Guna.UI2.WinForms.Guna2DataGridView();
            this.dataGridView_UnasignedGroups = new Guna.UI2.WinForms.Guna2DataGridView();
            this.tableLayoutParentPanel.SuspendLayout();
            this.tableLayoutTitlePanel.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Projectdetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AssignedGroups)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UnasignedG_Details)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UnasignedGroups)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutParentPanel
            // 
            this.tableLayoutParentPanel.ColumnCount = 3;
            this.tableLayoutParentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutParentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.Controls.Add(this.tableLayoutTitlePanel, 1, 1);
            this.tableLayoutParentPanel.Controls.Add(this.tableLayoutPanel7, 1, 2);
            this.tableLayoutParentPanel.Controls.Add(this.tableLayoutPanel1, 1, 3);
            this.tableLayoutParentPanel.Controls.Add(this.tableLayoutPanel2, 1, 4);
            this.tableLayoutParentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutParentPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutParentPanel.Name = "tableLayoutParentPanel";
            this.tableLayoutParentPanel.RowCount = 6;
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.322147F));
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.54362F));
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutParentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutParentPanel.Size = new System.Drawing.Size(1166, 745);
            this.tableLayoutParentPanel.TabIndex = 0;
            // 
            // tableLayoutTitlePanel
            // 
            this.tableLayoutTitlePanel.ColumnCount = 3;
            this.tableLayoutTitlePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutTitlePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutTitlePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutTitlePanel.Controls.Add(this.label7, 1, 0);
            this.tableLayoutTitlePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutTitlePanel.Location = new System.Drawing.Point(61, 40);
            this.tableLayoutTitlePanel.Name = "tableLayoutTitlePanel";
            this.tableLayoutTitlePanel.RowCount = 1;
            this.tableLayoutTitlePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTitlePanel.Size = new System.Drawing.Size(1043, 56);
            this.tableLayoutTitlePanel.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Nirmala UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label7.Location = new System.Drawing.Point(350, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(341, 56);
            this.label7.TabIndex = 2;
            this.label7.Text = "Manage Group Projects";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 6;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel7.Controls.Add(this.industryAdvisor_txt, 4, 2);
            this.tableLayoutPanel7.Controls.Add(this.label3, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label5, 3, 2);
            this.tableLayoutPanel7.Controls.Add(this.description_txt, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.projectTitle_Combobox, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.mainAdvisor_txt, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.co_Advisor_txt, 4, 1);
            this.tableLayoutPanel7.Controls.Add(this.AssignProject_btn, 4, 3);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(61, 102);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1043, 229);
            this.tableLayoutPanel7.TabIndex = 2;
            // 
            // industryAdvisor_txt
            // 
            this.industryAdvisor_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.industryAdvisor_txt.DefaultText = "";
            this.industryAdvisor_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.industryAdvisor_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.industryAdvisor_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.industryAdvisor_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.industryAdvisor_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.industryAdvisor_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.industryAdvisor_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.industryAdvisor_txt.Location = new System.Drawing.Point(731, 117);
            this.industryAdvisor_txt.Name = "industryAdvisor_txt";
            this.industryAdvisor_txt.PasswordChar = '\0';
            this.industryAdvisor_txt.PlaceholderText = "Industry Advisor";
            this.industryAdvisor_txt.SelectedText = "";
            this.industryAdvisor_txt.Size = new System.Drawing.Size(200, 36);
            this.industryAdvisor_txt.TabIndex = 51;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(523, 78);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(78, 15);
            this.label3.TabIndex = 40;
            this.label3.Text = "Co-Advisor";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 78);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 38;
            this.label1.Text = "Description";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(523, 135);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label5.Size = new System.Drawing.Size(105, 15);
            this.label5.TabIndex = 42;
            this.label5.Text = "Industry Advisor";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // description_txt
            // 
            this.description_txt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.description_txt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.description_txt.Location = new System.Drawing.Point(211, 60);
            this.description_txt.Name = "description_txt";
            this.description_txt.Size = new System.Drawing.Size(254, 51);
            this.description_txt.TabIndex = 44;
            this.description_txt.Text = "";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(523, 21);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 41;
            this.label4.Text = "Main Advisor";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 21);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(81, 15);
            this.label2.TabIndex = 47;
            this.label2.Text = "Project Title";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // projectTitle_Combobox
            // 
            this.projectTitle_Combobox.BackColor = System.Drawing.Color.Transparent;
            this.projectTitle_Combobox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.projectTitle_Combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectTitle_Combobox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.projectTitle_Combobox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.projectTitle_Combobox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.projectTitle_Combobox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.projectTitle_Combobox.ItemHeight = 30;
            this.projectTitle_Combobox.Location = new System.Drawing.Point(211, 3);
            this.projectTitle_Combobox.Name = "projectTitle_Combobox";
            this.projectTitle_Combobox.Size = new System.Drawing.Size(186, 36);
            this.projectTitle_Combobox.TabIndex = 49;
            this.projectTitle_Combobox.SelectedIndexChanged += new System.EventHandler(this.projectTitle_Combobox_SelectedIndexChanged);
            // 
            // mainAdvisor_txt
            // 
            this.mainAdvisor_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mainAdvisor_txt.DefaultText = "";
            this.mainAdvisor_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.mainAdvisor_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.mainAdvisor_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.mainAdvisor_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.mainAdvisor_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.mainAdvisor_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mainAdvisor_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.mainAdvisor_txt.Location = new System.Drawing.Point(731, 3);
            this.mainAdvisor_txt.Name = "mainAdvisor_txt";
            this.mainAdvisor_txt.PasswordChar = '\0';
            this.mainAdvisor_txt.PlaceholderText = "Main Advisor";
            this.mainAdvisor_txt.SelectedText = "";
            this.mainAdvisor_txt.Size = new System.Drawing.Size(200, 36);
            this.mainAdvisor_txt.TabIndex = 50;
            // 
            // co_Advisor_txt
            // 
            this.co_Advisor_txt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.co_Advisor_txt.DefaultText = "";
            this.co_Advisor_txt.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.co_Advisor_txt.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.co_Advisor_txt.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.co_Advisor_txt.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.co_Advisor_txt.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.co_Advisor_txt.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.co_Advisor_txt.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.co_Advisor_txt.Location = new System.Drawing.Point(731, 60);
            this.co_Advisor_txt.Name = "co_Advisor_txt";
            this.co_Advisor_txt.PasswordChar = '\0';
            this.co_Advisor_txt.PlaceholderText = "Co-Advisor";
            this.co_Advisor_txt.SelectedText = "";
            this.co_Advisor_txt.Size = new System.Drawing.Size(200, 36);
            this.co_Advisor_txt.TabIndex = 52;
            // 
            // AssignProject_btn
            // 
            this.AssignProject_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AssignProject_btn.BorderColor = System.Drawing.Color.Blue;
            this.AssignProject_btn.BorderRadius = 5;
            this.AssignProject_btn.BorderThickness = 1;
            this.AssignProject_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AssignProject_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AssignProject_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AssignProject_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AssignProject_btn.FillColor = System.Drawing.Color.White;
            this.AssignProject_btn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssignProject_btn.ForeColor = System.Drawing.Color.Black;
            this.AssignProject_btn.Location = new System.Drawing.Point(841, 174);
            this.AssignProject_btn.Name = "AssignProject_btn";
            this.AssignProject_btn.Size = new System.Drawing.Size(144, 32);
            this.AssignProject_btn.TabIndex = 48;
            this.AssignProject_btn.Text = "Assign Project";
            this.AssignProject_btn.Click += new System.EventHandler(this.AssignProject_btn_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridView_Projectdetails, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView_AssignedGroups, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(61, 337);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.22222F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.77778F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1043, 180);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // dataGridView_Projectdetails
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView_Projectdetails.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Projectdetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_Projectdetails.ColumnHeadersHeight = 4;
            this.dataGridView_Projectdetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_Projectdetails.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_Projectdetails.Dock = System.Windows.Forms.DockStyle.Right;
            this.dataGridView_Projectdetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_Projectdetails.Location = new System.Drawing.Point(544, 3);
            this.dataGridView_Projectdetails.Name = "dataGridView_Projectdetails";
            this.dataGridView_Projectdetails.RowHeadersVisible = false;
            this.dataGridView_Projectdetails.Size = new System.Drawing.Size(496, 142);
            this.dataGridView_Projectdetails.TabIndex = 1;
            this.dataGridView_Projectdetails.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_Projectdetails.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dataGridView_Projectdetails.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dataGridView_Projectdetails.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dataGridView_Projectdetails.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dataGridView_Projectdetails.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_Projectdetails.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_Projectdetails.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.dataGridView_Projectdetails.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView_Projectdetails.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_Projectdetails.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dataGridView_Projectdetails.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dataGridView_Projectdetails.ThemeStyle.HeaderStyle.Height = 4;
            this.dataGridView_Projectdetails.ThemeStyle.ReadOnly = false;
            this.dataGridView_Projectdetails.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_Projectdetails.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView_Projectdetails.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_Projectdetails.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_Projectdetails.ThemeStyle.RowsStyle.Height = 22;
            this.dataGridView_Projectdetails.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_Projectdetails.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Nirmala UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label6.Location = new System.Drawing.Point(3, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(515, 32);
            this.label6.TabIndex = 2;
            this.label6.Text = "Un Asigned Groups";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView_AssignedGroups
            // 
            this.dataGridView_AssignedGroups.AllowUserToAddRows = false;
            this.dataGridView_AssignedGroups.AllowUserToDeleteRows = false;
            this.dataGridView_AssignedGroups.AllowUserToResizeColumns = false;
            this.dataGridView_AssignedGroups.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dataGridView_AssignedGroups.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_AssignedGroups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView_AssignedGroups.ColumnHeadersHeight = 15;
            this.dataGridView_AssignedGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_AssignedGroups.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView_AssignedGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_AssignedGroups.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_AssignedGroups.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_AssignedGroups.Name = "dataGridView_AssignedGroups";
            this.dataGridView_AssignedGroups.ReadOnly = true;
            this.dataGridView_AssignedGroups.RowHeadersVisible = false;
            this.dataGridView_AssignedGroups.Size = new System.Drawing.Size(515, 142);
            this.dataGridView_AssignedGroups.TabIndex = 3;
            this.dataGridView_AssignedGroups.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_AssignedGroups.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dataGridView_AssignedGroups.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dataGridView_AssignedGroups.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dataGridView_AssignedGroups.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dataGridView_AssignedGroups.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_AssignedGroups.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_AssignedGroups.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.dataGridView_AssignedGroups.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView_AssignedGroups.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_AssignedGroups.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dataGridView_AssignedGroups.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dataGridView_AssignedGroups.ThemeStyle.HeaderStyle.Height = 15;
            this.dataGridView_AssignedGroups.ThemeStyle.ReadOnly = true;
            this.dataGridView_AssignedGroups.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_AssignedGroups.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView_AssignedGroups.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_AssignedGroups.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_AssignedGroups.ThemeStyle.RowsStyle.Height = 22;
            this.dataGridView_AssignedGroups.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_AssignedGroups.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_AssignedGroups.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_AssignedGroups_CellContentClick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.dataGridView_UnasignedG_Details, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.dataGridView_UnasignedGroups, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(61, 523);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1043, 180);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // dataGridView_UnasignedG_Details
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedG_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_UnasignedG_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView_UnasignedG_Details.ColumnHeadersHeight = 4;
            this.dataGridView_UnasignedG_Details.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_UnasignedG_Details.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView_UnasignedG_Details.Dock = System.Windows.Forms.DockStyle.Right;
            this.dataGridView_UnasignedG_Details.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedG_Details.Location = new System.Drawing.Point(544, 3);
            this.dataGridView_UnasignedG_Details.Name = "dataGridView_UnasignedG_Details";
            this.dataGridView_UnasignedG_Details.RowHeadersVisible = false;
            this.dataGridView_UnasignedG_Details.Size = new System.Drawing.Size(496, 138);
            this.dataGridView_UnasignedG_Details.TabIndex = 1;
            this.dataGridView_UnasignedG_Details.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedG_Details.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dataGridView_UnasignedG_Details.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dataGridView_UnasignedG_Details.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dataGridView_UnasignedG_Details.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dataGridView_UnasignedG_Details.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedG_Details.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedG_Details.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedG_Details.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView_UnasignedG_Details.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_UnasignedG_Details.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedG_Details.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dataGridView_UnasignedG_Details.ThemeStyle.HeaderStyle.Height = 4;
            this.dataGridView_UnasignedG_Details.ThemeStyle.ReadOnly = false;
            this.dataGridView_UnasignedG_Details.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedG_Details.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView_UnasignedG_Details.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_UnasignedG_Details.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_UnasignedG_Details.ThemeStyle.RowsStyle.Height = 22;
            this.dataGridView_UnasignedG_Details.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedG_Details.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // dataGridView_UnasignedGroups
            // 
            this.dataGridView_UnasignedGroups.AllowUserToAddRows = false;
            this.dataGridView_UnasignedGroups.AllowUserToDeleteRows = false;
            this.dataGridView_UnasignedGroups.AllowUserToResizeColumns = false;
            this.dataGridView_UnasignedGroups.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedGroups.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_UnasignedGroups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView_UnasignedGroups.ColumnHeadersHeight = 15;
            this.dataGridView_UnasignedGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_UnasignedGroups.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView_UnasignedGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_UnasignedGroups.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedGroups.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_UnasignedGroups.Name = "dataGridView_UnasignedGroups";
            this.dataGridView_UnasignedGroups.ReadOnly = true;
            this.dataGridView_UnasignedGroups.RowHeadersVisible = false;
            this.dataGridView_UnasignedGroups.Size = new System.Drawing.Size(515, 138);
            this.dataGridView_UnasignedGroups.TabIndex = 4;
            this.dataGridView_UnasignedGroups.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedGroups.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dataGridView_UnasignedGroups.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dataGridView_UnasignedGroups.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dataGridView_UnasignedGroups.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dataGridView_UnasignedGroups.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedGroups.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedGroups.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedGroups.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView_UnasignedGroups.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_UnasignedGroups.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedGroups.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dataGridView_UnasignedGroups.ThemeStyle.HeaderStyle.Height = 15;
            this.dataGridView_UnasignedGroups.ThemeStyle.ReadOnly = true;
            this.dataGridView_UnasignedGroups.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_UnasignedGroups.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView_UnasignedGroups.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_UnasignedGroups.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_UnasignedGroups.ThemeStyle.RowsStyle.Height = 22;
            this.dataGridView_UnasignedGroups.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dataGridView_UnasignedGroups.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dataGridView_UnasignedGroups.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_UnasignedGroups_CellContentClick);
            // 
            // UC_AssignProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutParentPanel);
            this.Name = "UC_AssignProject";
            this.Size = new System.Drawing.Size(1166, 745);
            this.tableLayoutParentPanel.ResumeLayout(false);
            this.tableLayoutTitlePanel.ResumeLayout(false);
            this.tableLayoutTitlePanel.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Projectdetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AssignedGroups)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UnasignedG_Details)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UnasignedGroups)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutParentPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTitlePanel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox description_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2Button AssignProject_btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Guna.UI2.WinForms.Guna2DataGridView dataGridView_Projectdetails;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Guna.UI2.WinForms.Guna2DataGridView dataGridView_UnasignedG_Details;
        private Guna.UI2.WinForms.Guna2ComboBox projectTitle_Combobox;
        private Guna.UI2.WinForms.Guna2TextBox industryAdvisor_txt;
        private Guna.UI2.WinForms.Guna2TextBox mainAdvisor_txt;
        private Guna.UI2.WinForms.Guna2TextBox co_Advisor_txt;
        private Guna.UI2.WinForms.Guna2DataGridView dataGridView_AssignedGroups;
        private Guna.UI2.WinForms.Guna2DataGridView dataGridView_UnasignedGroups;
    }
}
