﻿using MID_Project_FYP_Management.Helper_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MID_Project_FYP_Management.UC_Forms
{
    public partial class UC_ManageProject_CRUD : UserControl
    {
        public UC_ManageProject_CRUD()
        {
            InitializeComponent();
        }

        private void addProj_btn_Click(object sender, EventArgs e)
        {
            var uc = new UC_ProjectAdd();
            UserControl_Helper.addUserControl(uc, panelForm_Parent);
        }

        private void assignAdvisor_btn_Click(object sender, EventArgs e)
        {
            //var uc = new UC_AssignAdvisors_Project();
            //UserControl_Helper.addUserControl(uc, panelForm_Parent);
        }
    }
}
